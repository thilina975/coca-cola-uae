<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\Answer;
class Answer extends Model
{
    use HasFactory;
    public function questions()
    {
        return $this->belongsTo(Question::class,'questions_id');
    }
}
