<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\QuestionType;
class Question extends Model
{
    use HasFactory;
    public function questionType()
    {
        return $this->belongsTo(QuestionType::class,'question_types_id');
    }
}
