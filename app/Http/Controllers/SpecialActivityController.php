<?php

namespace App\Http\Controllers;

use App\Models\SpcialActivityImages;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File;
use Image;

class SpecialActivityController extends Controller
{
    public function special_activity(Request $request)
    {
        $user_id = session()->get('FRONT_USER_ID_COLA');
        $result['user_info'] = DB::table('users')->where(['id' => $user_id])->first();

        $result['outlet'] = DB::table('stores')->where(['id' => $request->get('oid')])->get();

        $spa = DB::table('spcial_activities')
            ->leftJoin('question_types', 'spcial_activities.qs_type', '=', 'question_types.id')
            ->select('spcial_activities.*', 'question_types.question_type')
            ->get();

            $spcial_activities =[];
            foreach ($spa as $data) {
                $acimagesalredy = DB::table('spcial_activity_images')->where(['qs' => $data->id])->where(['team' => $result['user_info']->teams_id])->count();
               
              
                if($acimagesalredy > 0){
                     $spcial_activity_images = DB::table('spcial_activity_images')->where(['qs' => $data->id])->where(['team' => $result['user_info']->teams_id])->get(); 
                   $im_count = count($spcial_activity_images);
                }else{
                      $spcial_activity_images = '';
                      $im_count = 0;
                }
               
                $spcial_activities[] =[
                   'id' => $data->id,
                   'question'=> $data->question,
                   'type' => $data->question_type,
                   'answer' =>  $spcial_activity_images,
                   'im_count' =>  $im_count,
                ];
            }

            $result['spcial_activities'] = $spcial_activities;
     

    //  dd($result['spcial_activities']);

        return view('spcial_activity', $result);
    }

    public function special_activity_im_save(Request $request)
    {

        // dd($request);

        $user_id = session()->get('FRONT_USER_ID_COLA');
        $user_info = DB::table('users')->where(['id' => $user_id])->first();

        try {

            // $model = new SpcialActivityTeamAnswer();
            // $model->question = $team_id;
            // $model->qs_type = $outletId;
            // $model->answers_id = $answerId;
            // $model->questions_id = $questionId;
            // $model->is_completed = 1;
            // $model->save();

            if ($request->hasfile('spim1')) {

                $request->validate([
                    'spim1' => 'nullable',
                ]);

            $imx =1;
                foreach ($request->file('spim1') as $file) {

                    $x = 70;

                    $name = $file->getClientOriginalName();
                    $only_image_name = explode('.', $name)[0];

                    $ext = $file->extension();
                    $imname = $user_info->teams_id . '-team-selfie-' . time() .$imx++. '.' . $ext;

                    $img_resize = Image::make($file->getRealPath());
                    // $img_resize->resize(800, 500);
                    // $img_resize->orientate();
                    
                    // $img_resize->resize(1000, 667, function ($constraint) {
                    //     $constraint->aspectRatio();
                    // });
                    // $img_resize->orientate();
                    $img_resize->save(public_path('upload/special-activity/' . $imname), $x);

                    $fileModal = new SpcialActivityImages();
                    $fileModal->team = $user_info->teams_id;
                    $fileModal->qs = 1;
                    $fileModal->image = $imname;
                    $fileModal->save();

                }
            }

            if ($request->hasfile('spim2')) {

                $request->validate([
                    'spim2' => 'nullable',
                    'spim2.*' => 'mimes:jpeg,jpg,png',
                ]);

                $imt = 1;
                foreach ($request->file('spim2') as $file) {

                    $x = 70;

                    $name = $file->getClientOriginalName();
                    $only_image_name = explode('.', $name)[0];

                    $ext = $file->extension();
                    $imname = $user_info->teams_id.'-best-outlet-' . time() .$imt++. '.' . $ext;

                    $img_resize = Image::make($file->getRealPath());
                    // $img_resize->resize(800, 500);
                    $img_resize->save(public_path('upload/special-activity/' . $imname), $x);

                    $fileModal = new SpcialActivityImages();
                    $fileModal->team = $user_info->teams_id;
                    $fileModal->qs = 2;
                    $fileModal->image = $imname;
                    $fileModal->save();

                }
            }

            $request->session()->flash('message', 'Imaged Uploaded!');
            return redirect()->back();

        } catch (Exception $e) {
            $err = $e->getMessage();

            $request->session()->flash('error', "Failed!");
            return redirect()->back();
        }

    }


    public function special_activity_im_delete(Request $request)
    {

        $pid = $request->post('imid');

        $request->validate([
            'imid' => 'required',
            'imurl' => 'required',
        ]);
        try {

            $url = $request->imurl;
            $image_path = "/upload/special-activity/$url";
            
            if (File::exists(public_path($image_path))) {
                File::delete(public_path($image_path));
                // @unlink($image_path);
            }
           
            $model = SpcialActivityImages::find($pid);
            $model->delete();
            $request->session()->flash('message', "Successfully Deleted");

            return redirect()->back();

        } catch (Exception $e) {
            $err = $e->getMessage();
   
            $request->session()->flash('error', "Something went wrong!");

            return redirect()->back();

        }

    }

}
