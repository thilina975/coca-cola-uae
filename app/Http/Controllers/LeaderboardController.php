<?php

namespace App\Http\Controllers;

use App\Models\UserScore;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class LeaderboardController extends Controller
{
    public function leaderboard(Request $request)
    {
      
        
        //  $result['team_scores'] = DB::table('team_scores')
        // ->leftJoin('teams', 'team_scores.team_id', '=', 'teams.id')
        // ->orderBy('team_scores.score', 'desc')
        // ->select('team_scores.score', 'teams.name')
        // ->get();

        $teams = DB::table('teams')->get();

        
        $all_teams=[];
        foreach($teams  as $key => $val){
            $sc = DB::table('team_scores')->where(['team_id' => $val->id])->first();
              
           $score ='';
           if($sc == ''){
            $score =0; 
           }else{
            $score = $sc->score; 
           }

            $all_teams[] = [
                'team'=>$val->name,
                'score'=>$score,
            ];
        }

        $keys = array_column($all_teams, 'score');

        array_multisort($keys, SORT_DESC, $all_teams);

        $result['team_scores'] = $all_teams;

        // dd($result['team_scores']);
        
        return view('leaderboard', $result);
    }



    public function leaderboard_cocacola(Request $request)
    {
      
        $teams = DB::table('teams')->get();

        
        $all_teams=[];
        foreach($teams  as $key => $val){
            $sc = DB::table('team_scores')->where(['team_id' => $val->id])->first();
              
           $score ='';
           if($sc == ''){
            $score =0; 
           }else{
            $score = $sc->score; 
           }

            $all_teams[] = [
                'team'=>$val->name,
                'score'=>$score,
            ];
        }

        $keys = array_column($all_teams, 'score');

        array_multisort($keys, SORT_DESC, $all_teams);

        $result['team_scores'] = $all_teams;

    
        
        return view('leaderboard-view', $result);
    }


    
}
