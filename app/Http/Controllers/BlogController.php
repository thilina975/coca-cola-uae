<?php

namespace App\Http\Controllers;

use App\Models\Newsfeed;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File;
use Image;

class BlogController extends Controller
{
    public function blog(Request $request)
    {
        $user_id = session()->get('FRONT_USER_ID_COLA');
        $user_info = DB::table('users')->where(['id' => $user_id])->first();

        $result['newsfeeds_my'] = DB::table('newsfeeds')
            ->where(['status' => 1])
            ->where(['user_id' => $user_id])
            ->orderBy('id', 'desc')
            ->get();

        // $result['newsfeeds_other'] = Newsfeed::where(['status' => 1])->where('user_id', '!=', $user_id)
        //     ->get();

              $result['newsfeeds_other'] = DB::table('newsfeeds')
        ->leftJoin('users', 'newsfeeds.user_id', '=', 'users.id')
        ->leftJoin('teams', 'users.teams_id', '=', 'teams.id')
        ->where(['newsfeeds.status' => 1])
        ->orderBy('newsfeeds.id', 'desc')
        ->select('newsfeeds.*',  'users.fname', 'users.lname', 'teams.name')
        ->get();


        

        return view('blog', $result);
    }

    public function add_post_img(Request $request)
    {

        $user_id = session()->get('FRONT_USER_ID_COLA');
        $request->validate([
            'images' => 'nullable',
            'images.*' => 'mimes:jpeg,jpg,webp,png',
        ]);

        try {
            if ($request->hasfile('images')) {

                foreach ($request->file('images') as $file) {

                    $x = 70;

                    $name = $file->getClientOriginalName();
                    $only_image_name = explode('.', $name)[0];

                    $ext = $file->extension();
                    $imname = $only_image_name . '-' . time() . '.' . $ext;

                    $img_resize = Image::make($file->getRealPath());
                    // $img_resize->resize(800, 500);
                    $img_resize->orientate();
                    $img_resize->save(public_path('upload/blog/' . $imname), $x);

                    $fileModal = new Newsfeed();
                    $fileModal->image = $imname;
                    $fileModal->user_id = $user_id;
                    $fileModal->description = $request->caption;
                    $fileModal->status = 1;
                    $fileModal->save();
                    
                }
            }

            $request->session()->flash('message', "Successfully Post");

            return redirect()->back();

        } catch (Exception $e) {
            $err = $e->getMessage();
            $request->session()->flash('error', "Something went wrong!");

            return redirect()->back();

        }

    }

    public function postdelete(Request $request)
    {

        $pid = $request->post('id');
        try {

            $url = $request->url;
            $image_path = "/upload/blog/$url";
            
            if (File::exists(public_path($image_path))) {
                File::delete(public_path($image_path));
                // @unlink($image_path);
            }
           
            $model = Newsfeed::find($pid);
            $model->delete();
            $request->session()->flash('message', "Successfully Deleted");

            return redirect()->back();

        } catch (Exception $e) {
            $err = $e->getMessage();
            dd($err);
            $request->session()->flash('error', "Something went wrong!");

            return redirect()->back();

        }

    }

}
