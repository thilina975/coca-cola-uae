<?php

namespace App\Http\Controllers;

use Crypt;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;

class LoginController extends Controller
{

    public function login(Request $request)
    {
        $user_id = session()->get('FRONT_USER_ID_COLA');

        if ($user_id == '') {
            return view('login');
        } else {
            return redirect('/leaderboard');
        }

    }

    public function login_process(Request $request)
    {
        $pw = Crypt::encrypt($request->login_password);

        $valid = Validator::make($request->all(), [
            "email" => 'required',
            "login_password" => 'required',

        ],
        );

        if (!$valid->passes()) {
            return response()->json(['status' => 'error', 'error' => $valid->errors()->toArray()]);
        } else {

            $result = DB::table('users')
                ->where(['email' => $request->email])
                ->get();
            if (isset($result[0])) {
                // $db_pwd = Crypt::decrypt($result[0]->password);
                $db_pwd = $result[0]->password;
                $status = $result[0]->status;

                if ($status == 0) {
                    $status = "error";
                    return response()->json(['status' => $status, 'msg' => '<div class="alert alert-danger mt-3 text-center">Your account has been deactivated</div>']);
                }

                if ($db_pwd == $request->login_password) {

                    $request->session()->put('FRONT_USER_LOGIN_COLA', true);
                    $request->session()->put('FRONT_USER_ID_COLA', $result[0]->id);
                    $request->session()->put('FRONT_USER_NAME_COLA', $result[0]->fname);
                    $status = "success";
                    $msg = "";
                } else {
                    $status = "error";
                    $msg = "<div class='alert alert-danger text-center' mt-3 role='alert'>Please enter valid password</div>";
                }

            } else {

                $status = "error";
                $msg = "<div class='alert alert-danger mt-3 text-center' role='alert'>Please enter valid Email ID</div>";
            }
            return response()->json(['status' => $status, 'msg' => $msg]);
        }
    }

}
