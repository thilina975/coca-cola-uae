<?php

namespace App\Http\Controllers;

use App\Models\Answer;
use App\Models\OutletType;
use App\Models\QImage;
use App\Models\Question;
use App\Models\StoresHasQuestion;
use App\Models\TeamScore;
use App\Models\OutletScore;

use App\Models\Store;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Image;

class AnswersController extends Controller
{

    public function show(Request $request)
    {
        if ($request->get('qid') !== null) {
            $qid = $request->get('qid');
        } else {
            return redirect('/outlets');
        }

        $result['activity_details'] = Question::where(['id' => $request->get('qid')])
            ->with(['questionType'])
            ->first();

        $result['question_answers_list'] = Answer::where(['questions_id' => $request->get('qid')])->get();

        $result['outlet'] = DB::table('stores')->where(['id' => $request->get('oid')])->first();

        $result['already'] = DB::table('stores_has_questions')
            ->where(['stores_id' => $request->get('oid')])
            ->where(['questions_id' => $request->get('qid')])
            ->first();

        if ($result['already'] != '') {
            if ($result['activity_details']->questionType->question_type == 'FILE' || $result['activity_details']->questionType->question_type == 'RADIO_AND_IMAGE') {

                $result['asnimages'] = DB::table('q_images')->where(['shq_id' => $result['already']->id])->get();

            }
        }

        $result['outlet_types'] = OutletType::get();
        return view('view-activity', $result);
    }

    public function activity_answer(Request $request)
    {

        $already = DB::table('stores')->where(['id' => $request->id])->where(['is_task_completed' => 1])->first();

        if ($already == '') {

            try {

                $user_id = session()->get('FRONT_USER_ID_COLA');
                $team = DB::table('users')->where(['id' => $user_id])->first();
                $name = $request->input();
                $outletId = $request->outlet_id;
                $getoutletname = DB::table('stores')->where(['id' => $outletId])->first();
                
                $outletname = strtolower(str_replace(" ", "_", $getoutletname->name));



                 $allpoints = 0;
                foreach ($name as $key => $data) {
                    //   echo $key.' - '. $data.'<br>';
                    if ($key != '_token' && $key != 'outlet_id') {

                        $answerId = $data;
                        $questionId = $key;
                    

                        $already = DB::table('stores_has_questions')
                            ->where(['stores_id' => $outletId])
                            ->where(['questions_id' => $questionId])
                            ->first();
                        $points = DB::table('answers')->where(['id' => $answerId])->first();

                        if ($points == '') {
                            $point = 0;
                        } else {
                            $point = $points->points;
                        }

                        $allpoints += $point;

                   

                        $sctmalready = DB::table('team_scores')->where(['team_id' => $team->teams_id])->first();

                        if ($sctmalready == '') {
                            $tmsc = new TeamScore();
                            $tmsc->team_id = $team->teams_id;
                            $tmsc->score = $point;
                            $tmsc->save();
                        } else {

                            if ($already == '') {
                                $tmtotalpoint = '';
                                $tmalpoint = $sctmalready->score;
                                $tmtotalpoint = $tmalpoint + $point;
                                $ussc = TeamScore::find($sctmalready->id);
                                $ussc->score = $tmtotalpoint;
                                $ussc->save();
                            } else {
                                $anscore = DB::table('answers')->where(['id' => $already->answers_id])->first();

                                $tmtotalpoint = '';
                                $tmalpoint = $sctmalready->score;
                                $tmtotalpoint = $tmalpoint + $point - $anscore->points;
                                $ussc = TeamScore::find($sctmalready->id);
                                $ussc->score = $tmtotalpoint;
                                $ussc->save();
                            }
                        }

                        if ($already == '') {
                            $model = new StoresHasQuestion();

                        } else {

                            if ($already->users_id == $user_id) {
                                $model = StoresHasQuestion::find($already->id);

                            } else {

                                $request->session()->flash('error', "Already given the answer");
                                return redirect()->back();

                            }

                        }

                        $model->users_id = $user_id;
                        $model->stores_id = $outletId;
                        $model->answers_id = $answerId;
                        $model->questions_id = $questionId;
                        $model->is_completed = 1;
                        $model->save();


                       

                        // $clid = '';
                        if($questionId == 5){
                            $clid = $model->id;

                        }
                        if($questionId == 10){
                            $clid10 = $model->id;

                        }
                        if($questionId == 11){
                            $clid11 = $model->id;

                        }
                        if($questionId == 12){
                            $clid12 = $model->id;

                        }  if($questionId == 13){
                            $clid13 = $model->id;

                        }

                    }
                }
                $otalready = DB::table('outlet_scores')->where(['outlet_id' => $outletId])->first();

                if ($otalready == '') {
                $otsc = new OutletScore();
                $otsc->score = $allpoints;
                $otsc->outlet_id = $outletId;
                $otsc->team_id = $team->teams_id;
                $otsc->save();
                }else{
                    $otsc = OutletScore:: find($otalready->id);
                    $otsc->score = $allpoints;
                    $otsc->outlet_id = $outletId;
                    $otsc->team_id = $team->teams_id;
                    $otsc->save();
                }

              
                if ($request->hasfile('imagefile_co')) {
                    foreach ($request->file('imagefile_co') as $file) {

                        $x = 70;

                        $name = $file->getClientOriginalName();
                        $only_image_name = explode('.', $name)[0];

                        $ext = $file->extension();
                        $imname = $team->teams_id .'-'. $outletname . '-cooler' . '.' . $ext;

                        $img_resize = Image::make($file->getRealPath());
                        // $img_resize->resize(800, 500);
                        // $img_resize->resize(1000, 667, function ($constraint) {
                        //     $constraint->aspectRatio();
                        // });
                        // $img_resize->orientate();
                        $img_resize->save(public_path('upload/question/' . $imname), $x);

                        $fileModal = new QImage();
                        $fileModal->image = $imname;
                        $fileModal->shq_id = $clid;
                        $fileModal->save();

                    }

                }

               
                if ($request->hasfile('imageFile10')) {

                   foreach ($request->file('imageFile10') as $file) {

                        $x = 70;

                        $name = $file->getClientOriginalName();
                        $only_image_name = explode('.', $name)[0];

                        $ext = $file->extension();
                        $imname = $team->teams_id . '-'.$outletname . '-selfie.' . $ext;

                        $img_resize = Image::make($file->getRealPath());
                        // $img_resize->resize(800, 500);
                        // $img_resize->resize(1000, 667, function ($constraint) {
                        //     $constraint->aspectRatio();
                        // });
                        // $img_resize->orientate();
                        $img_resize->save(public_path('upload/question/' . $imname), $x);

                        $fileModal = new QImage();
                        $fileModal->image = $imname;
                        $fileModal->shq_id = $clid10;
                        $fileModal->save();

                    }
                }

                if ($request->hasfile('imageFile11')) {

                    foreach ($request->file('imageFile11') as $file) {
 
                         $x = 70;
 
                         $name = $file->getClientOriginalName();
                         $only_image_name = explode('.', $name)[0];
 
                         $ext = $file->extension();
                         $imname = $team->teams_id . '-'.$outletname . '-rack.' . $ext;
 
                         $img_resize = Image::make($file->getRealPath());
                         // $img_resize->resize(800, 500);
                        //  $img_resize->resize(1000, 667, function ($constraint) {
                        //      $constraint->aspectRatio();
                        //  });
                        //  $img_resize->orientate();
                         $img_resize->save(public_path('upload/question/' . $imname), $x);
 
                         $fileModal = new QImage();
                         $fileModal->image = $imname;
                         $fileModal->shq_id = $clid11;
                         $fileModal->save();
 
                     }
                 }

                 if ($request->hasfile('imageFile12')) {

                    foreach ($request->file('imageFile12') as $file) {
 
                         $x = 70;
 
                         $name = $file->getClientOriginalName();
                         $only_image_name = explode('.', $name)[0];
 
                         $ext = $file->extension();
                         $imname = $team->teams_id . '-'.$outletname . '-posm.' . $ext;
 
                         $img_resize = Image::make($file->getRealPath());
                         // $img_resize->resize(800, 500);
                        //  $img_resize->resize(1000, 667, function ($constraint) {
                        //      $constraint->aspectRatio();
                        //  });
                        //  $img_resize->orientate();
                         $img_resize->save(public_path('upload/question/' . $imname), $x);
 
                         $fileModal = new QImage();
                         $fileModal->image = $imname;
                         $fileModal->shq_id = $clid12;
                         $fileModal->save();
 
                     }
                 }

                 if ($request->hasfile('imageFile13')) {

                    foreach ($request->file('imageFile13') as $file) {
 
                         $x = 70;
 
                         $name = $file->getClientOriginalName();
                         $only_image_name = explode('.', $name)[0];
 
                         $ext = $file->extension();
                         $imname = $team->teams_id . '-'.$outletname . '-warm-display.' . $ext;
 
                         $img_resize = Image::make($file->getRealPath());
                         // $img_resize->resize(800, 500);
                        //  $img_resize->resize(1000, 667, function ($constraint) {
                        //      $constraint->aspectRatio();
                        //  });
                        //  $img_resize->orientate();
                         $img_resize->save(public_path('upload/question/' . $imname), $x);
 
                         $fileModal = new QImage();
                         $fileModal->image = $imname;
                         $fileModal->shq_id = $clid13;
                         $fileModal->save();
 
                     }
                 }

                $smodel = Store::find($outletId);
                $smodel->is_task_completed = 1;
                $smodel->task_submitted_user_id = $user_id;

                $smodel->save();


                $request->session()->flash('message', "Successfully saved");

                // return redirect('/outlet/' . $outletId);

                return redirect('/outlets');

            } catch (Exception $e) {
                $err = $e->getMessage();
                $request->session()->flash('error', "Something went wrong!");

                return redirect()->back();
            }

        } else {

            $request->session()->flash('error', 'Outlet Already Submited');
            return redirect()->back();

        }

    }

}
