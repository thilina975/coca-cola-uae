<?php

namespace App\Http\Controllers\Admin;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Area;
use App\Models\District;
use Illuminate\Support\Facades\DB;
use Exception;
class LocationController extends Controller
{
    public function locations(Request $request)
    {

        // $result['areas']=Area::all();
        $result['district']=District::all();
        $result['areas'] = DB::table('areas')
            ->Join('districts', 'districts.id', '=', 'areas.district_id')
            ->select('areas.*', 'districts.district')
            ->get();
        $result['countrys'] = DB::table('countrys')->get();
        return view('admin.locations',$result);
    }

    public function ad_district(Request $request)
    {
        $already = DB::table('districts')
        ->where(['district' => $request->district])
        ->first();

    if (isset($already)) {
        $request->session()->flash('message', "<div class='alert alert-danger' role='alert'><i class='far fa-check-circle'></i> District Already Exists !</div>");
        return redirect()->back();
        
        }else{
    $model = new District();
    $model->district = $request->district;
    $model->country_id = $request->country;
    $model->status = 1;
    $model->save();

    $request->session()->flash('message', "<div class='alert alert-success' role='alert'><i class='far fa-check-circle'></i> District Added!</div>");
    return redirect()->back();
}
    }


    public function ad_area(Request $request)
    {
        $already = DB::table('areas')
        ->where(['area' => $request->area])
        ->first();

    if (isset($already)) {
        $request->session()->flash('message', "<div class='alert alert-danger' role='alert'><i class='far fa-check-circle'></i> Area Already Exists !</div>");
        return redirect()->back();
        
        }else{
    $model = new Area();
    $model->area = $request->area;
    $model->status = 1;
    $model->district_id = $request->district;
    $model->save();

    $request->session()->flash('message', "<div class='alert alert-success' role='alert'><i class='far fa-check-circle'></i> Area Added!</div>");
    return redirect()->back();
}
    }


    public function district_delete(Request $request)
    {

        $uid = $request->post('id');
    
        $model = District::find($uid);
        $model->delete();
        $request->session()->flash('message', "<div class='alert alert-success' role='alert'><i class='far fa-check-circle'></i> District Deleted!</div>");
        return redirect()->back();

    }

    public function area_delete(Request $request)
    {

        $uid = $request->post('id');
    
        $model = Area::find($uid);
        $model->delete();
        $request->session()->flash('message', "<div class='alert alert-success' role='alert'><i class='far fa-check-circle'></i> Area Deleted!</div>");
        return redirect()->back();

    }

    public function district_status(Request $request)
    {
        try {
            $user_tatus = District::where('id', $request->id)->first();
            $user_tatus->status = $request->status;
            $user_tatus->save();

            $request->session()->flash('message', "<div class='alert alert-success' role='alert'><i class='far fa-check-circle'></i> District Status Changed!</div>");
            return redirect()->back();
        } catch (Exception $e) {
            $err = $e->getMessage();
            $request->session()->flash('message', "<div class='alert alert-danger' role='alert'> District Status Change Failed!</div>");
            return redirect()->back();
        }

    }

    public function area_status(Request $request)
    {
        try {
            $user_tatus = Area::where('id', $request->id)->first();
            $user_tatus->status = $request->status;
            $user_tatus->save();

            $request->session()->flash('message', "<div class='alert alert-success' role='alert'><i class='far fa-check-circle'></i> Area Status Changed!</div>");
            return redirect()->back();
        } catch (Exception $e) {
            $err = $e->getMessage();
            $request->session()->flash('message', "<div class='alert alert-danger' role='alert'> Area Status Change Failed!</div>");
            return redirect()->back();
        }

    }

}