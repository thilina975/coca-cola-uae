<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Models\User;
class AdminHomeController extends Controller
{
    public function dashboard(Request $request , $id = '')
    {

        $result['users'] = DB::table('users')->get();
        $result['teams'] = DB::table('teams')->get();





        return view('admin.add-user', $result);
    }

    public function ad_price(Request $request)
    {

        DB::table('r_condition')
            ->where('id', 1)
            ->update(['ad_price' => $request->amount]);

        $request->session()->flash('message', "<div class='alert alert-success' role='alert'><i class='far fa-check-circle'></i> Ad Price Updated!</div>");
        return redirect()->back();
    }

    public function home_category(Request $request)
    {

        DB::table('r_condition')
            ->where('id', 1)
            ->update([
                'home_ct_1' => $request->category_1,
                'home_ct_2' => $request->category_2,
            ]);

        $request->session()->flash('message', "<div class='alert alert-success' role='alert'><i class='far fa-check-circle'></i> Home Ad Category Updated!</div>");
        return redirect()->back();
    }

    public function add_user(Request $request)
    {

        $already = DB::table('users')->where(['email' => $request->email])->first();

        if ($already == '') {

            try {

                if ($request->post('id') > 0) {
                    $model = User::find($request->post('id'));
         
        
                } else {
                    $otsc = new User();
                  
                }
 
                $otsc->fname = $request->fname;
                $otsc->lname = $request->lname;
                $otsc->email = $request->email;
                $otsc->teams_id = $request->team;
                $otsc->password = $request->password;
                $otsc->save();

                $request->session()->flash('message', "Successfully saved");

                // return redirect('/outlet/' . $outletId);

                return redirect()->back();

            } catch (Exception $e) {
                $err = $e->getMessage();
                $request->session()->flash('error', "Something went wrong!");

                return redirect()->back();
            }

        } else {

            $request->session()->flash('error', 'Outlet Already Submited');
            return redirect()->back();

        }

    }

}
