<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Models\User;

class AdminUserController extends Controller
{
    public function users(Request $request, $id = '')
    {

        if ($id > 0) {
            $arr = User::where(['id' => $id])->get();

            $result['fname'] = $arr['0']->fname;
            $result['lname'] = $arr['0']->lname;
            $result['email'] = $arr['0']->email;
            $result['password'] = $arr['0']->password;
            $result['team'] = $arr['0']->teams_id;
            $result['id'] = $arr['0']->id;

        } else {
            $result['fname'] = '';
            $result['lname'] =  '';
            $result['email'] =  '';
            $result['password'] =  '';
            $result['team'] =  '';
            $result['id'] = '';
        }


        $result['users'] = DB::table('users')->get();
        $result['teams'] = DB::table('teams')->get();


        return view('admin.add-user', $result);
    }


    public function add_user(Request $request)
    {

        $already = DB::table('users')->where(['email' => $request->email])->first();

        $request->validate([
            'email' => 'required|unique:users,email,' . $request->id,

        ]);

            try {

                if ($request->id > 0) {
                    
                    $otsc = User::find($request->post('id'));
         
        
                } else {
                    $otsc = new User();
                  
                }
 
                $otsc->fname = $request->fname;
                $otsc->lname = $request->lname;
                $otsc->email = $request->email;
                $otsc->teams_id = $request->team;
                $otsc->password = $request->password;
                $otsc->save();

                $request->session()->flash('message', "Successfully saved");

                // return redirect('/outlet/' . $outletId);

                return redirect()->back();

            } catch (Exception $e) {
                $err = $e->getMessage();
                $request->session()->flash('error', "Something went wrong!");

                return redirect()->back();
            }

     

    }
}
