<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\User;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\DB;

class UserController extends Controller
{
    public function users(Request $request)
    {

        $result['data'] = User::all();
        return view('admin.users', $result);
    }

    public function usersstatus(Request $request)
    {
        try {
            $user_tatus = User::where('id', $request->blockuid)->first();
            $user_tatus->status = $request->status;
            $user_tatus->save();
            if ($request->status == 1) {
                $ustatus = "Active";
            } else {
                $ustatus = "Blocked";
            }
            return response()->json(['status' => 'success', 'uid' => $request->blockuid, 'ustatus' => $ustatus, 'msg' => "<div class='alert alert-success' role='alert'><i class='far fa-check-circle'></i> User Status Changed !</div>"]);

        } catch (Exception $e) {
            $err = $e->getMessage();
            return response()->json(['status' => 'error', 'msg' => ""]);
        }

    }

    public function userprofile(Request $request, $id)
    {
        $result['user'] = DB::table('users')->where(['id' => $id])->first();
        $result['all_posts'] = DB::table('posts')
            ->Join('categories', 'categories.id', '=', 'posts.id')
            ->where(['posts.posted_by' => $id])
            ->select('posts.*', 'categories.name')
            ->get();

        foreach ($result['all_posts'] as $list1) {
            $result['post_image'][$list1->id] = DB::table('post_images')->where(['posts_idposts' => $list1->id])->get();
            if ($result['post_image'][$list1->id]->count() == '0') {
                $result['post_image'][$list1->id] = '';
            }
        }
        return view('admin.user', $result);
    }

    public function verifyemail(Request $request)
    {
        try {
            //    $request->blockuid;
            // dd($request->blockuid);
            $user_tatus = User::where('id', $request->uid)->first();
            $user_tatus->is_email_varified = 1;
            $user_tatus->save();

            $request->session()->flash('message', "<div class='alert alert-success' role='alert'><i class='far fa-check-circle'></i> Email Verified !</div>");
            return redirect()->back();

        } catch (Exception $e) {
            $err = $e->getMessage();
            $request->session()->flash('message', "<div class='alert alert-danger' role='alert'> Email Verification failed</div>");
            return redirect()->back();
        }

    }


    public function userdelete(Request $request)
    {

        $uid = $request->post('id');
    
        $model = User::find($uid);
        $model->delete();
        $request->session()->flash('message', "<div class='alert alert-success' role='alert'><i class='far fa-check-circle'></i> User Deleted!</div>");
        return redirect()->back();

    }


    public function user(Request $request)
    {

        // dd("awa");

        $request->validate([
            'name' => 'required',
            'mobile_number' => 'required|unique:users,mobile_number,' . $request->id,
            'email' => 'required|unique:users,email,' . $request->id,
            "land_number" => 'nullable|numeric|digits:10',
            "nic" => 'required',

        ]);

        try {
     
            $model = User::find($request->id);
            $model->name = $request->name;
            $model->email = $request->email;
            $model->mobile_number = $request->mobile_number;
            $model->land_number = $request->land_number;
            $model->nic = $request->nic;

            if ($request->hasfile('imageFile')) {
                foreach ($request->file('imageFile') as $file) {

                    $name = $file->getClientOriginalName();
                    $only_image_name = explode('.', $name)[0];

                    $ext = $file->extension();
                    $imname = $only_image_name . '-' . time() . '.' . $ext;
                    $file->move(public_path() . '/images/users', $imname);

                    $url = $request->post('url');
                    $image_path = "/images/users/$url";

                    if (File::exists(public_path($image_path))) {
                        File::delete(public_path($image_path));

                    }

                    $model->avatar = $imname;

                }
            }
            $model->save();

            // $model = User::where("id", $request->id)
            // ->update([
            //     "name" => $request->name,
            //     "email" => $request->email,
            //     "mobile_number" => $request->mobile_number,
            //     "land_number" => $request->land_number,
            //     "nic" => $request->nic,
            // ]);

            $request->session()->flash('message', "<div class='alert alert-success' role='alert'><i class='far fa-check-circle'></i> Profile Detail Updated!</div>");
            return redirect()->back();

        } catch (Exception $e) {
            $err = $e->getMessage();
             dd($err);
            $request->session()->flash('message', "<div class='alert alert-danger' role='alert'>Failed!</div>");
            return redirect()->back();
        }

    }


}
