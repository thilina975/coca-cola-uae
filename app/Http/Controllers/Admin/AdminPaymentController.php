<?php

namespace App\Http\Controllers\Admin;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Payment;
use Illuminate\Support\Facades\DB;
class AdminPaymentController extends Controller
{
    public function payments(Request $request)
    {

       $result['data']= DB::table('payments')
        ->Join('posts', 'posts.id', '=', 'payments.posts_idposts')
        ->Join('users', 'users.id', '=', 'payments.users_idusers')
        ->select('payments.*', 'posts.title', 'users.email')
        ->get();
        return view('admin.payments',$result);
    }

}
