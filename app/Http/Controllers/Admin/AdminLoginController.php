<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Crypt;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Validator;

class AdminLoginController extends Controller
{
    public function admin(Request $request)
    {

        return view('admin.index');
    }

    public function adminlogin(Request $request)
    {

        // dd("post");

        $email = $request->post('email');
        $password = $request->post('login_password');

        // $result = DB::table('users')
        //     ->where(['email' => $request->email])
        //     ->get();
        if ($email == "thilina975@gmail.com") {


            // $db_pwd = Crypt::decrypt($result[0]->password);
            $db_pwd = "19911231";
     if ($db_pwd == $password) {
            $request->session()->put('adminalog_1991', true);
            $request->session()->put('admin_id_1991', 1000);


            return redirect('/admin/users');

        } else {
            $request->session()->flash('error', "<div class='alert alert-danger' role='alert'>Please enter valid password</div>");
            return redirect()->back();
          
        }

        } else {
            $request->session()->flash('error', "<div class='alert alert-danger' role='alert'>Please enter valid login details</div>");
            return redirect()->back();
        }

    }

    public function fogot_password(Request $request)
    {

        $sid = session()->getId();
        $result = DB::collection('specialistaccounts')
            ->where(['email' => $request->email])
            ->first();

        //

        if (!empty($result)) {

            $request->session()->put('VENDOR_FP_EMAIL', $request->email);
            $request->session()->put('VENDOR_FP_CODE', $result['_id']);
            $request->session()->put('VENDOR_FP_ATTEMPT', '1');
            //    dd($request->email);
        }

        $login_api = Request::create('/apim/vendor/fogot_password', 'POST');
        $reponse = Route::dispatch($login_api);
        $result = json_decode($reponse->getContent(), true);

        //    dd($result);
        if ($result['status'] == "success") {
            return response()->json(['status' => 'success', 'msg' => "<div class='alert alert-success' role='alert'>Please check your email, and follow the instructions provided to login.</div>"]);
        } else {
            return response()->json(['status' => 'error', 'msg' => "<div class='alert alert-warning' role='alert'>Email id not registered.</div>"]);
        }

    }

    public function fogot_password_change(Request $request, $id)
    {

        $actual_code = session()->get('VENDOR_FP_CODE');

        // dd($actual_code);

        $result['user'] = DB::collection('specialistaccounts')
            ->where(['_id' => new \MongoDB\BSON\ObjectID($id)])
            ->get();

        if (isset($result['user'][0])) {
            //  dd($actual_code);
            $request->session()->put('MAGULLK_FORGOT_PASSWORD_VENDOR_ID', $id);
            return view('vendor.password_reset', $result);
        } else {
            return redirect('/');
        }

    }

    public function fogot_password_changing(Request $request)
    {

        $valid = Validator::make($request->all(), [
            'new_password' => 'min:6|required_with:confirm_password|same:confirm_password',
            'confirm_password' => 'min:6',
        ]);

        if (!$valid->passes()) {
            return response()->json(['status' => 'error', 'error' => $valid->errors()->toArray()]);
        } else {

            if ($request->id == '') {
                $user_id = $request->session()->get('MAGULLK_FORGOT_PASSWORD_VENDOR_ID');
            } else {
                $user_id = $request->id;
            }

            DB::collection('specialistaccounts')
                ->where(['_id' => new \MongoDB\BSON\ObjectID($user_id)])
                ->update(
                    [

                        'password_hash' => Hash::make($request->new_password),

                    ]
                );
            $request->session()->flash('message', "<div class='alert alert-success' role='alert'>Password Changed, Login with new password</div>");
            return response()->json(['status' => 'success', 'msg' => "<div class='alert alert-success' role='alert'>Password Changed</div>"]);
        }

    }

    public function vender_password_change(Request $request)
    {
        $valid = Validator::make($request->all(), [

            "old_password" => 'required',
            'password' => 'min:6|required_with:confirm_password|same:confirm_password',
            'confirm_password' => 'min:6',
        ]);

        if (!$valid->passes()) {
            return response()->json(['status' => 'error', 'error' => $valid->errors()->toArray()]);

        } else {
            $login_api = Request::create('/apim/vendor/password-change', 'POST');
            $reponse = Route::dispatch($login_api);
            $result = json_decode($reponse->getContent(), true);
            //    dd($result);

            if ($result['status'] == "success") {

                return response()->json(['status' => 'success', 'msg' => 'Password Updated']);
            } else {
                return response()->json(['status' => 'error', 'msg' => $result['msg']]);
            }

        }

    }

}
