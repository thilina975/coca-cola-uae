<?php

namespace App\Http\Controllers;

use App\Models\Answer;
use App\Models\Store;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class OutletsController extends Controller
{
    public function outlets(Request $request)
    {
        $user_id = session()->get('FRONT_USER_ID_COLA');
        $user_info = DB::table('users')->where(['id' => $user_id])->first();

        // $result['outlets'] = DB::table('stores')
        //     ->where(['teams_id' => $user_info->teams_id])
        //     ->get();

             $result['outlets'] = DB::table('stores')
        ->leftJoin('outlet_scores', 'outlet_scores.outlet_id', '=', 'stores.id')
        ->where(['stores.teams_id' => $user_info->teams_id])
        ->orderBy('stores.id', 'asc')
        ->select('stores.*', 'outlet_scores.score')
        ->get();
        

        return view('outlets', $result);
    }

    public function outlet(Request $request, $id)
    {

        $user_id = session()->get('FRONT_USER_ID_COLA');
        $user_info = DB::table('users')->where(['id' => $user_id])->first();

        $result['outlet'] = DB::table('stores')->where(['id' => $id])->first();
        $result['outlet_type'] = DB::table('outlet_types')->where(['id' => $result['outlet']->outlet_type_id])->first();
        $result['outlet_types'] = DB::table('outlet_types')->get();
        // $result['outlet'] = DB::table('stores')
        // ->leftJoin('outlet_types', 'stores.outlet_type_id', '=', 'outlet_types.id')
        // ->where(['stores.id' => $id])
        // ->select('stores.*', 'outlet_types.type')
        // ->get();

        $result['questions'] = DB::table('questions')->get();
        $questions = [];

        foreach ($result['questions'] as $data) {

            $point = Answer::where('questions_id', $data->id)->max('points');
            $type = DB::table('question_types')->where(['id' => $data->question_types_id])->first();
            $question_answers_list = DB::table('answers')->where(['questions_id' => $data->id])->get();

            $isTaskCompleted = DB::table('stores_has_questions')
                ->where(['questions_id' => $data->id])
                ->where(['stores_id' => $id])
                ->first();

            $completed = '';
            $result['sbtn'] = "";
            if ($isTaskCompleted == '') {
                $completed = 0;
                $result['sbtn'] = "disabled";
            } else {
                $completed = 1;
            }

            $ansalready = DB::table('stores_has_questions')
                ->where(['stores_id' => $id])
                ->where(['questions_id' => $data->id])
                ->first();
                 
                $answer ='';
                if($ansalready != ''){
                    $answer = $ansalready->answers_id;
                }
                // dd($ansalready->answers_id);
                $asnimages ='';
            if ($ansalready != '') {
                if ($type->question_type == 'FILE' || $type->question_type == 'RADIO_AND_IMAGE') {

                    $asnimages = DB::table('q_images')->where(['shq_id' => $ansalready->id])->get();

                }else{
                    $asnimages ='';
                }
            }

            $questions[] = [

                'id' => $data->id,
                'question' => $data->question,
                'type' => $type->question_type,
                'points' => $point,
                'answer_list' => $question_answers_list,
                'completed' => $completed,
                'answer' => $answer,
                'asnimages' => $asnimages,
            ];

        }
        $result['questions'] = $questions;
        // dd($result['questions']);

        return view('outlet', $result);
    }

    public function outlet_channel_set(Request $request)
    {

        try {

            $model = Store::find($request->id);
            $model->outlet_type_id = $request->outlet_type_id;
            $model->grade = $request->grade;
            $model->save();
            $request->session()->flash('message', 'Outlet Channel Updated');
            return redirect()->back();

        } catch (Exception $e) {
            $err = $e->getMessage();
            $request->session()->flash('error', "Failed!");
            return redirect()->back();
        }

    }

    public function submit_outlet(Request $request)
    {

        $user_id = session()->get('FRONT_USER_ID_COLA');

        $already = DB::table('stores')->where(['id' => $request->id])->where(['is_task_completed' => 1])->first();

        if ($already == '') {

            try {

                $model = Store::find($request->id);
                $model->is_task_completed = 1;
                $model->task_submitted_user_id = $user_id;

                $model->save();
                $request->session()->flash('message', 'Outlet Submited!');
                return redirect()->back();

            } catch (Exception $e) {
                $err = $e->getMessage();
                $request->session()->flash('error', "Failed!");
                return redirect()->back();
            }

        } else {

            $request->session()->flash('error', 'Outlet Already Submited');
            return redirect()->back();

        }

    }

}
