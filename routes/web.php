<?php

use Illuminate\Support\Facades\Route;

use App\Http\Controllers\LoginController;
use App\Http\Controllers\LeaderboardController;
use App\Http\Controllers\QuestionsController;
use App\Http\Controllers\OutletsController;
use App\Http\Controllers\AnswersController;
use App\Http\Controllers\SpecialActivityController;
use App\Http\Controllers\BlogController;

// Admin

use App\Http\Controllers\Admin\AdminLoginController;
use App\Http\Controllers\Admin\AdminUserController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('test', function () {
    return view('leaderboard-view');
});

Route::get('/pdf/agenda', function () {
    // file path
   $path = public_path('pdf/agenda.pdf');
    // header
   $header = [
     'Content-Type' => 'application/pdf',
     'Content-Disposition' => 'inline; filename="agenda.pdf"'
   ];
  return response()->file($path, $header);
})->name('pdf-agenda');


Route::get('/pdf/teams', function () {
    // file path
   $path = public_path('pdf/teams.pdf');
    // header
   $header = [
     'Content-Type' => 'application/pdf',
     'Content-Disposition' => 'inline; filename="teams.pdf"'
   ];
  return response()->file($path, $header);
})->name('pdf-teams');


Route::get('/', [LoginController::class, 'login']);
Route::post('/login_process', [LoginController::class, 'login_process']);


Route::get('/logout', function () { 
    session()->forget('FRONT_USER_LOGIN_COLA');
    session()->forget('FRONT_USER_ID_COLA');
    session()->forget('FRONT_USER_NAME_COLA');
    // Auth::logout();
    return redirect('/'); 
});


Route::get('/leaderboard-cocacola', [LeaderboardController::class, 'leaderboard_cocacola']);


// User dashboard

Route::group(['middleware'=>'user_auth'],function(){

    Route::get('/outlets', [OutletsController::class, 'outlets']);
    Route::get('/outlet/{id}', [OutletsController::class, 'outlet']);
    Route::post('/outlet-channel-set', [OutletsController::class, 'outlet_channel_set']);
    Route::post('/submit-outlet', [OutletsController::class, 'submit_outlet']);


    Route::get('/activity', [AnswersController::class, 'show']);
    Route::post('/activity-answer', [AnswersController::class, 'activity_answer']);

    Route::get('/leaderboard', [LeaderboardController::class, 'leaderboard']);

    Route::get('/blog', [BlogController::class, 'blog']);
    Route::post('/add-post-img', [BlogController::class, 'add_post_img']);

    Route::post('/postdelete', [BlogController::class, 'postdelete']);

    

    Route::get('/special-activity', [SpecialActivityController::class, 'special_activity']);
    Route::post('/special-activity-im-save', [SpecialActivityController::class, 'special_activity_im_save']);
    Route::post('/special-activity-im-delete', [SpecialActivityController::class, 'special_activity_im_delete']);
});


Route::get('/admin', [AdminLoginController::class, 'admin']);
Route::post('/adminlogin', [AdminLoginController::class, 'adminlogin']);


Route::group(['middleware'=>'admin_auth'],function(){
    Route::get('/admin/users', [AdminUserController::class, 'users']);
    Route::get('/admin/users/{id}', [AdminUserController::class, 'users']);
    Route::POST('/admin/add-users', [AdminUserController::class, 'add_user']);
});

Route::get('/admin/logout', function () { 
    session()->forget('adminalog_1991');
    session()->forget('admin_id_1991');
    return redirect('/admin'); 
});


Route::get('clear_cache', function () {
    \Artisan::call('config:cache');
    \Artisan::call('view:clear');
    \Artisan::call('route:clear');
    // dd("Cache is cleared");
});

