<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Special Activity</title>
    <!-- Favicon icon -->
    <link rel="icon" type="image/png" sizes="16x16" href="{{ asset('images/favicon.png')}}">
    <!-- Custom Stylesheet -->
    <link rel="stylesheet" href="{{ asset('assets/css/style.css')}}?<?php echo time(); ?>">
    <link rel="stylesheet" href="{{ asset('assets/css/custom.css')}}?<?php echo time(); ?>">
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/vendor/light-box/jquery.lightbox.css') }}">
    <script src="{{ asset('assets/vendor/jquery/jquery.min.js')}}"></script>

    
    <script>
    (function($) {

        $.fn.imageUploadResizer = function(options) {
            var settings = $.extend({
                max_width: 1000,
                max_height: 1000,
                quality: 1,
                do_not_resize: [],
            }, options);

            this.filter('input[type="file"]').each(function() {
                this.onchange = function() {
                    const that = this; // input node
                    const originalFile = this.files[0];

                    if (!originalFile || !originalFile.type.startsWith('image')) {
                        return;
                    }

                    // Don't resize if doNotResize is set
                    if (settings.do_not_resize.includes('*') || settings.do_not_resize.includes(
                            originalFile.type.split('/')[1])) {
                        return;
                    }

                    var reader = new FileReader();

                    reader.onload = function(e) {
                        var img = document.createElement('img');
                        var canvas = document.createElement('canvas');

                        img.src = e.target.result
                        img.onload = function() {
                            var ctx = canvas.getContext('2d');
                            ctx.drawImage(img, 0, 0);

                            if (img.width < settings.max_width && img.height < settings
                                .max_height) {
                                // Resize not required
                                return;
                            }

                            const ratio = Math.min(settings.max_width / img.width, settings
                                .max_height / img.height);
                            const width = Math.round(img.width * ratio);
                            const height = Math.round(img.height * ratio);

                            canvas.width = width;
                            canvas.height = height;

                            var ctx = canvas.getContext('2d');
                            ctx.drawImage(img, 0, 0, width, height);

                            canvas.toBlob(function(blob) {
                                var resizedFile = new File([blob], 'activity_' +
                                    originalFile.name, originalFile);

                                var dataTransfer = new DataTransfer();
                                dataTransfer.items.add(resizedFile);

                                // temporary remove event listener, change and restore
                                var currentOnChange = that.onchange;

                                that.onchange = null;
                                that.files = dataTransfer.files;
                                that.onchange = currentOnChange;

                            }, 'image/jpeg', settings.quality);
                        }
                    }

                    reader.readAsDataURL(originalFile);
                }
            });

            return this;
        };

    }(jQuery));
    </script>

    <style>
    .card .card-body {
        background: white;
    }

    .card {
        border: 1px solid #e5192e;
    }

    #preloader div {
        height: 100%;
        display: inherit;
        align-items: center;
        text-align: center;
        justify-content: center;
        margin-top: 40vh;
        color: white;
    }
    </style>
</head>

<body class="dashboard">

    <div id="preloader">
        <div><img src="{{ asset('images/loading.gif')}}">
            <p id="wait"></p>
        </div>

    </div>
    <div id="main-wrapper">



        @include('user-header')

        <div class="content-body">
            <div class="container">
                <div class="row">
                    <div class="col-xl-12">
                        <div class="page-title">
                            <div class="row align-items-center justify-content-between">



                                @include('login-user')





                            </div>
                        </div>
                    </div>


                    <div class="col-xxl-12 col-xl-12">

                        <div class="row">

                            <div class="col-xxl-12">
                                <div class="card">
                                    <div class="card-header">
                                        <h4 class="card-title">Special Activity </h4>

                                    </div>
                                    <div class="card-body">

                                        <form action="/special-activity-im-save" id="form-submit" method="POST"
                                            enctype="multipart/form-data">

                                            @CSRF
                                            <input type="hidden" value="{{$user_info->teams_id}}" name="teams_id">

                                            <div class="row bg-white justify-content-md-center">


                                                @foreach($spcial_activities as $sa)
                                                <div class="col-12 col-md-6 col-lg-6">



                                                    <div class="mb-4">
                                                        <h3>{{$sa['question']}}</h3>

                                                        <lable>Maximum 3 images</lable>
                                                        <div class="mb-3">
                                                            <input class="form-control mb-1" id="spim{{$sa['id']}}1"
                                                                name="spim{{$sa['id']}}[]" type="file"
                                                                accept="image/png, image/jpeg">
                                                            <input class="form-control mb-1" id="spim{{$sa['id']}}2"
                                                                name="spim{{$sa['id']}}[]" type="file"
                                                                accept="image/png, image/jpeg">
                                                            <input class="form-control" id="spim{{$sa['id']}}3"
                                                                name="spim{{$sa['id']}}[]" type="file"
                                                                accept="image/png, image/jpeg">
                                                        </div>

                                                    </div>

                                                    <script>
                                                    $('#spim<?php echo $sa['id'];?>1').imageUploadResizer({
                                                        max_width: 1000, // Defaults 1000
                                                        max_height: 800, // Defaults 1000
                                                        quality:1, // Defaults 1
                                                        do_not_resize: ['gif', 'svg'], // Defaults []
                                                    });
                                                    $('#spim<?php echo $sa['id'];?>2').imageUploadResizer({
                                                        max_width: 1000, // Defaults 1000
                                                        max_height:800, // Defaults 1000
                                                        quality: 1, // Defaults 1
                                                        do_not_resize: ['gif', 'svg'], // Defaults []
                                                    });
                                                    $('#spim<?php echo $sa['id'];?>3').imageUploadResizer({
                                                        max_width: 1000, // Defaults 1000
                                                        max_height: 800, // Defaults 1000
                                                        quality: 1, // Defaults 1
                                                        do_not_resize: ['gif', 'svg'], // Defaults []
                                                    });
                                                    </script>

                                                    @if(isset($sa['answer'][0]))
                                                    <div class="row  mt-5 mb-5">
                                                        @foreach($sa['answer'] as $data)
                                                        <div
                                                            class="col-xl-4 position-relative col-lg-4 col-md-4 col-sm-6">
                                                            <a class="delete-i"
                                                                onclick="deleteim('{{$data->id}}', '{{$data->image}}')"
                                                                href="javascript:void(0);"> <i
                                                                    class="ri-delete-bin-line"></i>
                                                            </a>


                                                            <div class="gallery">

                                                                <a href="{{ asset('upload/special-activity/'.$data->image.'') }}"
                                                                    target="_blank">
                                                                    <img src="{{ asset('upload/special-activity/'.$data->image.'') }}"
                                                                        class="img-fluid" />
                                                                </a>
                                                            </div>
                                                        </div>





                                                        @endforeach

                                                    </div>
                                                    @endif
                                                </div>

                                                <?php
$filecount = 3 - $sa['im_count'];
?>
                                                <script>
                                                $("#spim{{$sa['id']}}").on("change", function() {
                                                    if ($("#spim{{$sa['id']}}")[0].files.length >
                                                        <?php echo $filecount ?>) {
                                                        $('#exampleModal{{$sa['id']}}').modal('show');
                                                        $('#spim{{$sa['id']}}').val('');
                                                    }
                                                });
                                                </script>


                                                <div class="modal fade" id="exampleModal{{$sa['id']}}" tabindex="-1"
                                                    aria-labelledby="exampleModalLabel" aria-hidden="true">
                                                    <div class="modal-dialog modal-dialog-centered">
                                                        <div class="modal-content">

                                                            <div class="modal-body text-center">
                                                                <h1><i class="ri-error-warning-line text-danger"></i>
                                                                </h1>
                                                                <p>You can select only {{$filecount}} images</p>
                                                                <button type="button" class="btn btn-warning"
                                                                    data-bs-dismiss="modal">Ok</button>
                                                            </div>

                                                        </div>
                                                    </div>
                                                </div>
                                                @endforeach









                                                <div class="col-12 mt-3  mb-4">
                                                    <button type="submit" id="btnSave" onclick="myFunction()"
                                                        class="btn btn-danger">Upload</button>

                                                </div>






                                            </div>
                                            <!-- row body end -->
                                        </form>


                                    </div>
                                </div>
                            </div>





                        </div>
                    </div>

                </div>
            </div>
        </div>

    </div>


    <!-- User Delete Confirm -->
    <div class="modal fade" id="deleteim" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle"
        aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <form method="POST" action="/special-activity-im-delete">
                    @csrf
                    <input type="hidden" id="imidv" value="" name="imid">
                    <input type="hidden" id="imurlv" value="" name="imurl">
                    <div class="modal-body text-center bg-danger text-white">
                        Confirm to Delete
                    </div>
                    <div class="modal-footer" style="text-align: center;  display: inherit;">
                        <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">No</button>
                        <button type="submit" class="btn btn-danger">Yes</button>
                    </div>
                </form>

            </div>
        </div>
    </div>
    <!--End User delete Confirm -->






    @include('user-footer')

    <script src="{{ asset('assets/vendor/bootstrap/bootstrap.min.js')}}"></script>
    <script src="{{ asset('assets/vendor/bootstrap/popper.min.js')}}"></script>

    <script src="{{ asset('assets/js/scripts.js')}}"></script>

    <script src="{{ asset('js/custom.js')}}?<?php echo time(); ?>"></script>
    <script>
    function deleteim(id, url) {

        $("#imidv").val(id);

        $('#imurlv').val(url);
        $('#deleteim').modal('show');
    }
    </script>
    <script>
    var form = document.getElementById('form-submit');

    function myFunction() {
        if (form.checkValidity()) {
            $("#preloader").show();
            jQuery('#wait').html('Please wait, Uploading...');

        }
    }
    </script>

    <script src="{{ asset('assets/vendor/light-box/jquery.min.js')}}"></script>
    <script src="{{ asset('assets/vendor/light-box/jquery.lightbox.js')}}"></script>
    <script>
    $(function() {
        $('.gallery a').lightbox();
    });



    $("#spim2").on("change", function() {
        if ($("#spim2")[0].files.length > 3) {
            $('#exampleModal').modal('show');
            $('#spim2').val('');
        }
    });
    </script>

    @if(Session::has('message'))
    <script>
    $(document).ready(function() {
        $("#liveToast").toast('show');
    });
    </script>
    @endif

    <div class="position-fixed bottom-0 end-0 p-3" style="z-index: 11">

        <div class="toast align-items-center text-white bg-success border-0" role="alert" id="liveToast"
            aria-live="assertive" aria-atomic="true">
            <div class="d-flex">
                <div class="toast-body">
                    {!! session('message') !!}
                </div>
                <button type="button" class="btn-close btn-close-white me-2 m-auto" data-bs-dismiss="toast"
                    aria-label="Close"></button>
            </div>
        </div>

    </div>

    @if(Session::has('error'))
    <script>
    $(document).ready(function() {
        $("#liveToastre").toast('show');
    });
    </script>
    @endif

    <div class="position-fixed bottom-0 end-0 p-3" style="z-index: 11">

        <div class="toast align-items-center text-white bg-danger border-0" role="alert" id="liveToastre"
            aria-live="assertive" aria-atomic="true">
            <div class="d-flex">
                <div class="toast-body">
                    {!! session('error') !!}
                </div>
                <button type="button" class="btn-close btn-close-white me-2 m-auto" data-bs-dismiss="toast"
                    aria-label="Close"></button>
            </div>
        </div>

    </div>







</body>

</html>