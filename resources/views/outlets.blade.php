<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Outlets</title>
    <!-- Favicon icon -->
    <link rel="icon" type="image/png" sizes="16x16" href="{{ asset('images/favicon.png')}}">
    <!-- Custom Stylesheet -->
    <link rel="stylesheet" href="{{ asset('assets/css/style.css')}}?<?php echo time();?>">
    <link rel="stylesheet" href="{{ asset('assets/css/custom.css')}}?<?php echo time();?>">
    


</head>

<body class="dashboard">
<div id="preloader">
       <div><img src="{{ asset('images/loading.gif')}}"></div>
    </div>
    <div id="main-wrapper">



        @include('user-header')

        <div class="content-body">
            <div class="container">
                <div class="row">
                    <div class="col-xl-12">
                        <div class="page-title">
                            <div class="row align-items-center justify-content-between">



                                @include('login-user')





                            </div>
                        </div>
                    </div>


                    <div class="col-xxl-12 col-xl-12">

                        <div class="row">

                            <div class="col-xxl-12">
                                <div class="card">
                                    <div class="card-header">
                                        <h4 class="card-title">Outlets</h4>
                                    </div>
                                    <div class="card-body">
                                        <div class="row">

                                            @foreach($outlets as $data)
                                            <div class="col-xl-3 col-lg-3 col-md-4 col-sm-6 col-12 mb outlet" >
                                                <a href="/outlet/{{$data->id}}" >
                                                    <div class="balance-stats"
                                                     style="<?php if($data->is_task_completed == 1){
                                                         echo "background-image: linear-gradient(to right, #d8e9d7 , #deb94e);";
                                                        }
                                                         ?>"
                                                      id="inner_outlet">
                                                        
                                                        <h3>{{$data->name}} &nbsp;   @if($data->is_task_completed == 1)
                                                        <small class="submit"><i class="ri-checkbox-circle-line"></i></small>
                                                        @endif </h3>
                                                        <small class="address">{{$data->address}}</small>
                                                           <span class="score">{{$data->score}}</span>
                                                        <!-- <span id="icon_rotate" class="hi-icon hi-icon-archive">Archive</span> -->

                                                    </div>
                                                </a>
                                            </div>
                                            @endforeach
                                        </div>
                                    </div>
                                </div>
                            </div>






                        </div>
                    </div>

                </div>
            </div>
        </div>

    </div>




    @include('user-footer')


    <script src="{{ asset('assets/vendor/jquery/jquery.min.js')}}"></script>

    <script src="{{ asset('assets/vendor/bootstrap/bootstrap.min.js')}}"></script>
    <script src="{{ asset('assets/vendor/bootstrap/popper.min.js')}}"></script>
    <script src="{{ asset('assets/js/scripts.js')}}"></script>

    <script src="{{ asset('js/custom.js')}}?<?php echo time(); ?>"></script>




    @if(Session::has('message'))
    <script>
    $(document).ready(function() {
        $("#liveToast").toast('show');
    });
    </script>
    @endif

    <div class="position-fixed bottom-0 end-0 p-3" style="z-index: 11">

        <div class="toast align-items-center text-white bg-success border-0" role="alert" id="liveToast"
            aria-live="assertive" aria-atomic="true">
            <div class="d-flex">
                <div class="toast-body">
                    {!! session('message') !!}
                </div>
                <button type="button" class="btn-close btn-close-white me-2 m-auto" data-bs-dismiss="toast"
                    aria-label="Close"></button>
            </div>
        </div>

    </div>




</body>

</html>