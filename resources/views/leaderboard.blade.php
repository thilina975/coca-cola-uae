<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Leaderboard</title>
    <!-- Favicon icon -->
    <link rel="icon" type="image/png" sizes="16x16" href="{{ asset('images/favicon.png')}}">
    <!-- Custom Stylesheet -->
    <link rel="stylesheet" href="{{ asset('assets/css/style.css')}}?<?php echo time();?>">
    <link rel="stylesheet" href="{{ asset('assets/css/custom.css')}}?<?php echo time();?>">

    <style>
    .card .card-body {
        background: white;
    }

    .card {
        border: 1px solid #e5192e;
    }
    </style>
</head>

<body class="dashboard">

    <div id="preloader">
        <div><img src="{{ asset('images/loading.gif')}}"></div>
    </div>
    <div id="main-wrapper">



        @include('user-header')

        <div class="content-body">
            <div class="container">
                <div class="row">
                    <div class="col-xl-12">
                        <div class="page-title">
                            <div class="row align-items-center justify-content-between">



                                @include('login-user')





                            </div>
                        </div>
                    </div>


                    <div class="col-xxl-12 col-xl-12">

                        <div class="row">

                            <div class="col-xxl-12">


                                <div class="card">
                                    <div class="card-header">
                                        <h4 class="card-title">Leaderboard </h4>
                                    </div>
                                    <div class="card-body">
                                        @php
                                        $x=1;
                                        $i =1;
                                        @endphp
                                        @foreach($team_scores as $data)
                                         
                                        <div class="verify-content d-flex leader">
                                            <div class="d-flex align-items-center">
                                                <span class="me-3 icon-circle bg-primary text-white"><?php echo $x++;?></span>
                                                <div class="primary-number">
                                                    <p class="mb-0">{{$data['team']}}</p>

                                                </div>
                                          
                                                 @if($i++ == 1)
                                                <div>
                                                <i class="ri-trophy-line"></i>
                                                </div>
                                                @endif
                                               
                                            </div>
                                            <h5 class="m-0">{{$data['score']}}</h5>
                                            <!-- <button class=" btn btn-primary"></button> -->
                                        </div>
                                       

                                        @endforeach

                                    </div>
                                </div>




                            </div>






                        </div>
                    </div>

                </div>
            </div>
        </div>

    </div>




    @include('user-footer')


    <script src="{{ asset('assets/vendor/jquery/jquery.min.js')}}"></script>
    <script src="{{ asset('assets/vendor/bootstrap/bootstrap.min.js')}}"></script>
    <script src="{{ asset('assets/vendor/bootstrap/popper.min.js')}}"></script>
    <script src="{{ asset('assets/js/scripts.js')}}"></script>

    <script src="{{ asset('js/custom.js')}}?<?php echo time(); ?>"></script>




    @if(Session::has('message'))
    <script>
    $(document).ready(function() {
        $("#liveToast").toast('show');
    });
    </script>
    @endif

    <div class="position-fixed bottom-0 end-0 p-3" style="z-index: 11">

        <div class="toast align-items-center text-white bg-success border-0" role="alert" id="liveToast"
            aria-live="assertive" aria-atomic="true">
            <div class="d-flex">
                <div class="toast-body">
                    {!! session('message') !!}
                </div>
                <button type="button" class="btn-close btn-close-white me-2 m-auto" data-bs-dismiss="toast"
                    aria-label="Close"></button>
            </div>
        </div>

    </div>

    @if(Session::has('error'))
    <script>
    $(document).ready(function() {
        $("#liveToastre").toast('show');
    });
    </script>
    @endif

    <div class="position-fixed bottom-0 end-0 p-3" style="z-index: 11">

        <div class="toast align-items-center text-white bg-danger border-0" role="alert" id="liveToastre"
            aria-live="assertive" aria-atomic="true">
            <div class="d-flex">
                <div class="toast-body">
                    {!! session('error') !!}
                </div>
                <button type="button" class="btn-close btn-close-white me-2 m-auto" data-bs-dismiss="toast"
                    aria-label="Close"></button>
            </div>
        </div>

    </div>







</body>

</html>