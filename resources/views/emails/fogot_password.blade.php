<table align='center' bgcolor='#EFEEEA' border='0' cellpadding='0' cellspacing='0' height='100%' width='100%'>
    <tbody>
        <tr>
            <td align='center' valign='top' style='padding-bottom:60px'>
                <table align='center' border='0' cellpadding='0' cellspacing='0' width='100%'>
                    <tbody>
                        <tr>
                            <td align='center' valign='top'>
                                <table align='center' bgcolor='#FFFFFF' border='0' cellpadding='0' cellspacing='0' style=' border-top: 5px solid #f8c51a; background-color:#ffffff; color: #656565; max-width:640px; font-size: 15px;  font-family: system-ui;    border-radius: 10px; margin-top: 30px;' width='100%'>
                                    <tbody>
                                        <tr>
                                            <td align='center' valign='top' style='padding: 30px 0px 20px 0px;'>
                                                <a href='#' style='text-decoration:none' target='_blank'>
                                                <img src="{{ asset('images/logo.png')}}" style='width: 125px;'>
                                                </a>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td valign='top' bgcolor='#FFFFFF' style='padding-right:40px;padding-bottom:40px;padding-left:40px'>
                                                <h1 style='color: #656565; font-size: 20px; font-style:normal;font-weight:400;line-height:42px;letter-spacing:normal;margin:0;padding:0;text-align:center'>Forgot Password Reset!</h1>
                                                <p>Hello {{$name}},</p>
                                            
                                                <p style='margin: 5px auto;font-size: 15px;'>
                                                 There was a request to reset your Password. If you did not make this request, just ignore this email and check your account. Otherwise, please click the button below to reset your password.
                                                </p>


                                            </td>
                                        </tr>
                                        <tr>
                                            <td align='center' valign='middle' style='padding-right:40px;padding-bottom:40px;padding-left:40px'>

                                                <p>
                                                    <a href="{{url('/fogot_password_change/')}}/{{$sid}}" style='width: 230px;
                                                       background-color: #f8c619;
                                                       padding: 11px 20px;
                                                       display: block;
                                                       border-radius: .25rem;
                                                       font-size: 14px;
                                                       text-transform: uppercase;
                                                       color: white;
                                                       cursor: pointer;
                                                       text-decoration: none;'>
                                                       Reset Password
                                                    </a>
                                                </p>
  <p>If button is not working click this link below <br>
                                                    <a href="{{url('/fogot_password_change/')}}/{{$sid}}">{{url('/fogot_password_change/')}}/{{$sid}}</a>
                                                </p>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align='center' valign='top' style='border-top:2px solid #efeeea;color:#6a655f; font-size:12px;font-weight:400;line-height:24px;padding-top:20px;padding-bottom:20px;text-align:center'>
                                                <p style='color:#6a655f;font-size:12px;font-weight:400;line-height:24px;padding:0 20px;margin:0;text-align:center'>Situwara Pawura </p>

                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </td>
        </tr>
    </tbody>
</table>