<table align='center' bgcolor='#EFEEEA' border='0' cellpadding='0' cellspacing='0' height='100%' width='100%'>
    <tbody>
        <tr>
            <td align='center' valign='top' style='padding-bottom:60px'>
                <table align='center' border='0' cellpadding='0' cellspacing='0' width='100%'>
                    <tbody>
                        <tr>
                            <td align='center' valign='top'>
                                <table align='center' bgcolor='#FFFFFF' border='0' cellpadding='0' cellspacing='0' style='background-color:#ffffff;     border-top: 5px solid #f8c51a;
    border-radius: 10px; color: #656565; max-width:640px; font-size: 15px; font-family: system-ui; margin-top: 30px;' width='100%'>
                                    <tbody>
                                        <tr>
                                            <td align='center' valign='top' style='padding: 30px 0px 20px 0px;'>
                                                <a href='#' style='text-decoration:none' target='_blank'>
                                                <img src="{{ asset('images/logo.png')}}" style='width: 125px;'>
                                                </a>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td valign='top' bgcolor='#FFFFFF' style='padding-right:40px;text-align: center;padding-bottom:20px;padding-left:40px'>
                                             
                                                <h4 style='font-size: 20px; margin: 10px 0px 0px 0px;color: #f8c619;'>OTP Verification
                                                   
                                                </h4>
                                                <p style='margin: 15px auto;font-size: 15px; line-height: 20px;'>
                                                To continue with your email verification, Please enter the following code.
                                             </p>
                                                <h4 style='font-size: 18px; margin: 10px 0px 0px 0px;'>OTP: {{$code}}  </h4>
                                        
                                               
                                                
                                               
                                            </td>
                                        </tr>
                                       
                                        <tr>
                                            <td align='center' valign='top' style='border-top:2px solid #efeeea;color:#6a655f; font-size:12px;font-weight:400;line-height:24px;padding-top:20px;padding-bottom:20px;text-align:center'>
                                            <p style='margin: 15px auto;font-size: 13px; line-height: 20px;'>
                                                If this wasn't you, Please ignore this email or contact our customer service.    
                                             </p>    
                                            <p style='color:#6a655f;font-size:12px;font-weight:400;line-height:24px;padding:0 20px;margin:0;text-align:center'>ibeclk.org.<br>No.51, Gayan Building, Kandy Road, Yakkala, Sri Lanka.</p>

                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </td>
        </tr>
    </tbody>
</table>