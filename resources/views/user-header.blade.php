
@php
    $sid = session()->getId();
    $user_id = session()->get('FRONT_USER_ID_IBEC');
    $user_name = session()->get('FRONT_USER_NAME_IBEC');
    $user_info = DB::table('users')->where(['id' => $user_id])->first();
    @endphp
<div class="sidebar">
    <div class="brand-logo"><a class="full-logo" href="/"><img src="{{ asset('images/coca-cola.gif')}}" alt=""
                width="100%"></a>
    </div>
    <div class="menu">
        <ul>
            <li><a href="/outlets">
                    <span><i class="ri-home-8-line"></i></span>
                    <span class="nav-text">Home</span>
                </a>
            </li>
      
            <li><a href="/leaderboard">
                    <span><i class="ri-medal-line"></i></span>
                    <span class="nav-text">Leaderboard</span>
                </a>
            </li>
         
            <li><a href="/special-activity">
                    <span><i class="ri-star-line"></i></span>
                    <span class="nav-text">Activity</span>
                </a>
            </li>
            <li><a href="/blog">
                    <span><i class="ri-newspaper-line"></i></span>
                    <span class="nav-text">News</span>
                </a>
            </li>
          
            <!-- <li class="logout"><a href="/logout">
                    <span><i class="ri-logout-circle-line"></i></span>
                    <span class="nav-text">Signout</span>
                </a>
            </li> -->
        </ul>
    </div>
</div>