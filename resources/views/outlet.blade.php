<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Outlet</title>
    <!-- Favicon icon -->
    <link rel="icon" type="image/png" sizes="16x16" href="{{ asset('images/favicon.png')}}">
    <!-- Custom Stylesheet -->
    <link rel="stylesheet" href="{{ asset('assets/css/style.css')}}?<?php echo time();?>">
    <link rel="stylesheet" href="{{ asset('assets/css/custom.css')}}?<?php echo time();?>">
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/vendor/light-box/jquery.lightbox.css') }}">
    <style>
    .card .card-body {
        background: white;
    }

    .card {
        border: 1px solid #e5192e;
    }

    #preloader div {
        height: 100%;
        display: inherit;
        align-items: center;
        text-align: center;
        justify-content: center;
        margin-top: 40vh;
        color: white;
    }
    </style>
</head>

<body class="dashboard">

    <div id="preloader">
        <div><img src="{{ asset('images/loading.gif')}}">
            <p id="wait"></p>
        </div>

    </div>
    <div id="main-wrapper">



        @include('user-header')

        <div class="content-body">
            <div class="container">
                <div class="row">
                    <div class="col-xl-12">
                        <div class="page-title">
                            <div class="row align-items-center justify-content-between">



                                @include('login-user')





                            </div>
                        </div>
                    </div>
                    <style>
                    table {
                        border: 1px solid #ccc;
                        border-collapse: collapse;
                        margin: 0;
                        padding: 0;
                        width: 100%;
                        font-size: 13px;
                    }

                    table caption {
                        font-size: 1.5em;
                        margin: .5em 0 .75em;
                    }

                    table tr {
                        background-color: #f8f8f8;
                        border: 1px solid #ddd;
                        padding: 0 0.35em;
                    }

                    table th,
                    table td {
                        padding: .625em;
                    }

                    table th {
                        font-size: .85em;
                        letter-spacing: .1em;
                        text-transform: uppercase;
                    }

                    @media screen and (max-width: 600px) {
                        table {
                            border: 0;
                        }

                        table caption {
                            font-size: 1.3em;
                        }

                        table thead {
                            border: none;
                            clip: rect(0 0 0 0);
                            height: 1px;
                            margin: -1px;
                            overflow: hidden;
                            padding: 0;
                            position: absolute;
                            width: 1px;
                        }

                        table tr {
                            border-bottom: 1px solid #ddd;
                            display: block;
                            margin-bottom: 0.4em;
                        }

                        table td {
                            border-bottom: 1px solid #ddd;
                            display: block;
                            /* font-size: .8em; */
                            text-align: right;
                        }

                        table td::before {
                            /*
    * aria-label has no advantage, it won't be read inside a table
    content: attr(aria-label);
    */
                            content: attr(data-label);
                            float: left;

                        }

                        table td:last-child {
                            border-bottom: 0;
                        }
                    }
                    </style>

                    <div class="col-xxl-12 col-xl-12">

                        <div class="row">

                            <div class="col-xxl-12">
                                <div class="card">
                                    <div class="card-header">
                                        <h4 class="card-title">{{$outlet->name}} </h4>
                                        <small class="float-end text-white">Outlet Channel: @if($outlet_type== '') Not
                                            Selected

                                            @else
                                            {{$outlet_type->type}} | {{$outlet->grade}}

                                            @endif
                                            <!-- <button type="button" class="btn"
                                                style="border: 1px solid white; padding: 1px 10px;"
                                                data-bs-toggle="modal" data-bs-target="#settypemodal">
                                                <i class="ri-edit-box-line"></i>
                                            </button> -->
                                        </small>
                                    </div>
                                    <div class="card-body">
                                        <form action="/activity-answer" id="form-submit" method="POST"
                                            enctype="multipart/form-data">
                                            @CSRF


                                            <div class="row bg-white justify-content-md-center">
                                                <div class="col-md-8 col-12 m-p-0" style="    z-index: 10;">



                                                    <div class="table-responsive ">
                                                        <table class="table align-middle">
                                                            <thead>
                                                                <tr>

                                                                    <th scope="col">Question</th>


                                                                    <th scope="col">Answer</th>


                                                                </tr>
                                                            </thead>
                                                            <tbody>
                                                                @foreach($questions as $data)



                                                                <tr>

                                                                    <td style="text-align:left" class="qs">
                                                                        <span class="text-danger">
                                                                            {{$data['question']}}</span>
                                                                        <small>(pts {{$data['points']}})</small>
                                                                    </td>

                                                                    <td data-label="Answer">
                                                                        @if($data['type'] == 'DROPDOWN')
                                                                        <select name="{{$data['id']}}"
                                                                            class="form-control" >
                                                                            <option value="">Select Answer</option>
                                                                            @foreach($data['answer_list'] as $ans)
                                                                            <option
                                                                                <?php if($data['answer'] == $ans->id){echo 'selected';}?>
                                                                                value="{{$ans->id}}">
                                                                                {{$ans->answer}}
                                                                            </option>


                                                                            @endforeach
                                                                        </select>
                                                                        @elseif($data['type'] == 'RADIO')
                                                                        @foreach($data['answer_list'] as $ans)
                                                                        <div class="form-check form-check-inline">
                                                                            <input
                                                                                <?php if($data['answer'] == $ans->id){echo 'checked';}?>
                                                                                class="form-check-input" type="radio"
                                                                                value="{{$ans->id}}"
                                                                                name="{{$data['id']}}"
                                                                                id="{{$ans->id}}">
                                                                            <label class="form-check-label"
                                                                                for="{{$ans->id}}">
                                                                                {{$ans->answer}}</label>
                                                                        </div>
                                                                        @endforeach

                                                                        @elseif($data['type'] == 'RADIO_AND_IMAGE')

                                                                        @foreach($data['answer_list'] as $ans)
                                                                        <div class="form-check form-check-inline">
                                                                            <input
                                                                                <?php if($data['answer'] == $ans->id){echo 'checked';}?>
                                                                                class="form-check-input" type="radio"
                                                                                value="{{$ans->id}}"
                                                                                name="{{$data['id']}}"
                                                                                onclick="<?php
                                                             if ($ans->answer == "Yes") { echo "showFileInput()";   }else{echo "hideFileInput()";} ?> "
                                                                                id="{{$ans->id}}">
                                                                            <label class="form-check-label"
                                                                                for="{{$ans->id}}">
                                                                                {{$ans->answer}}</label>
                                                                        </div>
                                                                        @endforeach
                                                                        <input type="file" class="form-control mt-3"
                                                                            accept="image/png, image/jpeg"
                                                                            name="imagefile_co[]" id="imagefile_co"
                                                                            value="Show Div" style="display:none;">
                                                                        @if(isset($data['asnimages'][0]))
                                                                        <div class="gallery d-flex">
                                                                            @foreach($data['asnimages'] as $data)

                                                                            <div style="    width: 60px;
    padding: 4px;
    margin: 10px 0px;">
                                                                                <a href="{{ asset('upload/question/'.$data->image.'') }}"
                                                                                    target="_blank">
                                                                                    <img src="{{ asset('upload/question/'.$data->image.'') }}"
                                                                                        class="img-fluid" />
                                                                                </a>
                                                                            </div>

                                                                            @endforeach

                                                                        </div>
                                                                        @endif
                                                                        @elseif($data['type'] == 'FILE')

                                                                    
                                                                        <input type="hidden" name="{{$data['id']}}"
                                                                            value="{{$data['answer_list'][0]->id}}">
                                                                        <input type="file" id="imageFile{{$data['id']}}"
                                                                            accept="image/png, image/jpeg"
                                                                            class="form-control mb-1" name="imageFile{{$data['id']}}[]">
                                                                           
                                                                        @if(isset($data['asnimages'][0]))
                                                                        <div class="gallery d-flex">
                                                                            @foreach($data['asnimages'] as $data)

                                                                            <div style="    width: 60px; padding: 4px; margin: 10px 0px;">
                                                                                <a href="{{ asset('upload/question/'.$data->image.'') }}"
                                                                                    target="_blank">
                                                                                    <img src="{{ asset('upload/question/'.$data->image.'') }}"
                                                                                        class="img-fluid" />
                                                                                </a>
                                                                            </div>

                                                                            @endforeach

                                                                        </div>
                                                                        @endif


                                                                        @endif

                                                                    </td>


                                                                </tr>
                                                                @endforeach
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                </div>

                                                <div class="col-12 text-center  mt-3">

                                                    @if($outlet->is_task_completed == 0)


                                                    <input type="hidden" value="{{$outlet->id}}" name="outlet_id">
                                                    <button type="submit" onclick="myFunction()"
                                                        class="btn btn-primary btn-lg">Submit All
                                                        Task</button>


                                                    @else

                                                    <div class="alert alert-success " role="alert">

                                                        <h4> <i class="ri-checkbox-circle-line"></i> &nbsp;
                                                            Well done! <small>Outlet Submited</small>
                                                        </h4>
                                                    </div>


                                                    @endif
                                                </div>
                                                <!-- <h1 class="ot-bg">{{$outlet->name}}</h1> -->
                                            </div>
                                        </form>
                                    </div>



                                </div>
                            </div>






                        </div>
                    </div>

                </div>
            </div>
        </div>

    </div>

    <div class="modal fade" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered">
            <div class="modal-content">

                <div class="modal-body text-center">
                    <h1><i class="ri-error-warning-line text-danger"></i>
                    </h1>
                    <p>You can select only 4 images</p>
                    <button type="button" class="btn btn-warning" data-bs-dismiss="modal">Ok</button>
                </div>

            </div>
        </div>
    </div>



    @include('user-footer')

    <script src="{{ asset('assets/vendor/jquery/jquery.min.js')}}"></script>

    <script src="{{ asset('assets/vendor/bootstrap/bootstrap.min.js')}}"></script>
    <script src="{{ asset('assets/vendor/bootstrap/popper.min.js')}}"></script>
    <script src="{{ asset('assets/js/scripts.js')}}"></script>

    <script src="{{ asset('js/custom.js')}}?<?php echo time(); ?>"></script>

    <script src="{{ asset('assets/vendor/light-box/jquery.min.js')}}"></script>
    <script src="{{ asset('assets/vendor/light-box/jquery.lightbox.js')}}"></script>






    <script>

(function( $ ) {

$.fn.imageUploadResizer = function(options) {
    var settings = $.extend({
        max_width: 1000,
        max_height: 1000,
        quality: 1,
        do_not_resize: [],
    }, options );

    this.filter('input[type="file"]').each(function () {
        this.onchange = function() {
            const that = this; // input node
            const originalFile = this.files[0];

            if (!originalFile || !originalFile.type.startsWith('image')) {
                return;
            }

            // Don't resize if doNotResize is set
            if (settings.do_not_resize.includes('*') || settings.do_not_resize.includes( originalFile.type.split('/')[1])) {
                return;
            }

            var reader = new FileReader();

            reader.onload = function (e) {
                var img = document.createElement('img');
                var canvas = document.createElement('canvas');

                img.src = e.target.result
                img.onload = function () {
                    var ctx = canvas.getContext('2d');
                    ctx.drawImage(img, 0, 0);

                    if (img.width < settings.max_width && img.height < settings.max_height) {
                        // Resize not required
                        return;
                    }

                    const ratio = Math.min(settings.max_width / img.width, settings.max_height / img.height);
                    const width = Math.round(img.width * ratio);
                    const height = Math.round(img.height * ratio);

                    canvas.width = width;
                    canvas.height = height;

                    var ctx = canvas.getContext('2d');
                    ctx.drawImage(img, 0, 0, width, height);

                    canvas.toBlob(function (blob) {
                        var resizedFile = new File([blob], 'cocacola_'+originalFile.name, originalFile);

                        var dataTransfer = new DataTransfer();
                        dataTransfer.items.add(resizedFile);

                        // temporary remove event listener, change and restore
                        var currentOnChange = that.onchange;

                        that.onchange = null;
                        that.files = dataTransfer.files;
                        that.onchange = currentOnChange;

                    }, 'image/jpeg', settings.quality);
                }
            }

            reader.readAsDataURL(originalFile);
        }
    });

    return this;
};

}(jQuery));

$('#imagefile_co').imageUploadResizer({
        max_width: 800, // Defaults 1000
        max_height: 800, // Defaults 1000
        quality: 0.8, // Defaults 1
        do_not_resize: ['gif', 'svg'], // Defaults []
    });
    $('#imageFile10').imageUploadResizer({
        max_width: 800, // Defaults 1000
        max_height: 800, // Defaults 1000
        quality: 0.8, // Defaults 1
        do_not_resize: ['gif', 'svg'], // Defaults []
    });

    $('#imageFile11').imageUploadResizer({
        max_width: 800, // Defaults 1000
        max_height: 800, // Defaults 1000
        quality: 0.8, // Defaults 1
        do_not_resize: ['gif', 'svg'], // Defaults []
    });
    $('#imageFile12').imageUploadResizer({
        max_width: 800, // Defaults 1000
        max_height: 800, // Defaults 1000
        quality: 0.8, // Defaults 1
        do_not_resize: ['gif', 'svg'], // Defaults []
    });
    $('#imageFile13').imageUploadResizer({
        max_width: 800, // Defaults 1000
        max_height: 800, // Defaults 1000
        quality: 0.8, // Defaults 1
        do_not_resize: ['gif', 'svg'], // Defaults []
    });
</script>


    <script>
    // Initiate Lightbox
    $(function() {
        $('.gallery a').lightbox();
    });

    $("#imageFile").on("change", function() {
        if ($("#imageFile")[0].files.length > 4) {
            $('#exampleModal').modal('show');
            $('#imageFile').val('');
        }
    });

    var form = document.getElementById('form-submit');

    function myFunction() {
        if (form.checkValidity()) {
            $("#preloader").show();
            jQuery('#wait').html('Please wait while we process your request.');

        }
    }
    </script>

    <script>
    function showFileInput() {
        document.getElementById('imagefile_co').style.display = "block";
    }

    function hideFileInput() {
        document.getElementById('imagefile_co').style.display = "none";
    }
    </script>


    @if(Session::has('message'))
    <script>
    $(document).ready(function() {
        $("#liveToast").toast('show');
    });
    </script>
    @endif

    <div class="position-fixed bottom-0 end-0 p-3" style="z-index: 11">

        <div class="toast align-items-center text-white bg-success border-0" role="alert" id="liveToast"
            aria-live="assertive" aria-atomic="true">
            <div class="d-flex">
                <div class="toast-body">
                    {!! session('message') !!}
                </div>
                <button type="button" class="btn-close btn-close-white me-2 m-auto" data-bs-dismiss="toast"
                    aria-label="Close"></button>
            </div>
        </div>

    </div>

    @if(Session::has('error'))
    <script>
    $(document).ready(function() {
        $("#liveToastre").toast('show');
    });
    </script>
    @endif

    <div class="position-fixed bottom-0 end-0 p-3" style="z-index: 11">

        <div class="toast align-items-center text-white bg-danger border-0" role="alert" id="liveToastre"
            aria-live="assertive" aria-atomic="true">
            <div class="d-flex">
                <div class="toast-body">
                    {!! session('error') !!}
                </div>
                <button type="button" class="btn-close btn-close-white me-2 m-auto" data-bs-dismiss="toast"
                    aria-label="Close"></button>
            </div>
        </div>

    </div>



    <!-- Outlet type modal -->

    <div class="modal fade" id="settypemodal" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1"
        aria-labelledby="staticBackdropLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="staticBackdropLabel">Select Outlet Channel & Segment</h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <form action="/outlet-channel-set" method="POST">
                    @CSRF
                    <input type="hidden" value="{{$outlet->id}}" name="id">
                    <div class="modal-body">

                        <div class="col-12">

                            <select class="form-control" name="outlet_type_id" required>
                                <option value="">Select Outlet Channel</option>
                                @foreach($outlet_types as $data)
                                <option <?php if($outlet->outlet_type_id == $data->id){echo 'selected';}?>
                                    value="{{$data->id}}">{{$data->type}}</option>
                                @endforeach
                            </select>
                        </div>

                        <div class="col-12 mt-4">

                            <select class="form-control" name="grade" required>
                                <option value="">Select Outlet Segment </option>

                                <option <?php if($outlet->grade == "Gold"){echo 'selected';}?> value="Gold">Gold
                                </option>
                                <option <?php if($outlet->grade == "Silver"){echo 'selected';}?> value="Silver">Silver
                                </option>

                            </select>
                        </div>


                    </div>
                    <div class="modal-footer">
                        <!-- <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button> -->
                        <button type="submit" class="btn btn-primary">Save</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

    @if($outlet_type== '')
    <!-- <script>
    $(document).ready(function() {
        $('#settypemodal').modal('show');
    });
    </script> -->
    @endif
    <!-- End Outlet Type Modal -->





</body>

</html>