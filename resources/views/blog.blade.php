<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>News</title>
    <!-- Favicon icon -->
    <link rel="icon" type="image/png" sizes="16x16" href="{{ asset('images/favicon.png')}}">
    <!-- Custom Stylesheet -->
    <link rel="stylesheet" href="{{ asset('assets/css/style.css')}}?<?php echo time(); ?>">
    <link rel="stylesheet" href="{{ asset('assets/css/custom.css')}}?<?php echo time(); ?>">


    <link rel="stylesheet" type="text/css" href="{{ asset('assets/vendor/light-box/jquery.lightbox.css') }}">
    <style>
    .card .card-body {
        background: white;
    }

    .card {
        border: 1px solid #e5192e;
    }

    .image-uploader .uploaded .uploaded-image {

        width: calc(50% - 1rem);
        padding-bottom: calc(50% - 1rem);

    }
    .lazy {
  opacity: 0;
  transition: opacity 1s;
}
    #preloader div {
        height: 100%;
        display: inherit;
        align-items: center;
        text-align: center;
        justify-content: center;
        margin-top: 40vh;
        color: white;
    }
    </style>
</head>

<body class="dashboard">

    <div id="preloader">
        <div><img src="{{ asset('images/loading.gif')}}">
            <p id="wait"></p>
        </div>

    </div>
    <div id="main-wrapper">



        @include('user-header')

        <div class="content-body">
            <div class="container">
                <div class="row">
                    <div class="col-xl-12">
                        <div class="page-title">
                            <div class="row align-items-center justify-content-between">

                                @include('login-user')

                            </div>
                        </div>
                    </div>

                    <style>
                    #the-count {
                        padding: 0.1rem 0 0 0;
                        font-size: 0.7rem;
                        position: absolute;
                    }
                    </style>

                    <div class="col-xxl-12 col-xl-12">

                        <div class="row">

                            <div class="col-12 mb-4">


                                <div class="accordion accordion-flush" id="accordionFlushExample">
                                    <div class="accordion-item ">
                                        <h2 class="accordion-header bg-danger" id="flush-headingOne">
                                            <button class="accordion-button collapsed text-white" type="button"
                                                data-bs-toggle="collapse" data-bs-target="#flush-collapseOne"
                                                aria-expanded="false" aria-controls="flush-collapseOne">
                                                Create New Post
                                            </button>
                                        </h2>
                                        <div id="flush-collapseOne" class="accordion-collapse collapse"
                                            aria-labelledby="flush-headingOne" data-bs-parent="#accordionFlushExample">
                                            <div class="accordion-body">

                                                <div class="col-12">
                                                    <div class="row p-3">
                                                        <div class="col-lg-3 mb-4 col-sm-6 border">
                                                            <form action="/add-post-img" id="form-submit" method="POST"
                                                                enctype="multipart/form-data">
                                                                @CSRF
                                                                <h4 class="text-center m-0 pt-2 pb-3">Create New Post</h4>
                                                                <!-- <div class="input-field">
                                                                    <div class="input-images-1"
                                                                        style="padding-top: .5rem;">
                                                                    </div>
                                                                </div> -->

                                                                <div class="mb-3">
                                                                 
                                                                    <input class="form-control" required name="images[]"  accept="image/png, image/jpeg"  type="file"
                                                                        id="images-news">
                                                                </div>
                                                                <div class="text-center mt-3 col-12">
                                                                    <!-- <lable></lable> -->
                                                                    <textarea maxlength="150"
                                                                        placeholder="Your Post Caption"
                                                                        class="form-control" name="caption"></textarea>
                                                                    <div id="the-count">
                                                                        <span id="current">0</span>
                                                                        <span id="maximum">/ 150</span>
                                                                    </div>
                                                                </div>
                                                                <div class="text-center mt-3 mb-3 col-12">
                                                                    <button type="submit" id="btnSave"
                                                                        onclick="myFunction()"
                                                                        class="btn btn-danger btn-sm">Create
                                                                        Post</button>
                                                                </div>
                                                            </form>
                                                        </div>


                                                        <div class="col-lg-9">
                                                            <div class="row">

                                                                @foreach($newsfeeds_my as $data)

                                                                <div class="col-lg-4 mb-4 position-relative col-sm-6">
                                                                    <div class="card post-card">



                                                                        <div class="card-body">
                                                                            <div class="gallery">
                                                                                <a href="{{ asset('upload/blog/'.$data->image.'') }}"
                                                                                    target="_blank">



                                                                                    <div class="bg-image"
                                                                                        style="background-image:url({{ asset('upload/blog/'.$data->image.'') }})">
                                                                                    </div>

                                                                                    <div class="bg-text">

                                                                                
                                                                                        <img
                                                                                                src="{{ asset('upload/blog/'.$data->image.'') }}" />
                                                                                        

                                                                                    </div>

                                                                                </a>
                                                                            </div>
                                                                        </div>

                                                                        <div class="card-footer">
                                                                            <p class="card-text">{{$data->description}}
                                                                            </p>
                                                                        </div>

                                                                        <a class="delete-i" data-bs-toggle="modal"
                                                                            data-bs-target="#postdelete{{$data->id}}"
                                                                            href="javascript:void(0);"> <i
                                                                                class="ri-delete-bin-line"></i>
                                                                        </a>



                                                                        <!-- User Delete Confirm -->
                                                                        <div class="modal fade"
                                                                            id="postdelete{{$data->id}}" tabindex="-1"
                                                                            role="dialog"
                                                                            aria-labelledby="exampleModalCenterTitle"
                                                                            aria-hidden="true">
                                                                            <div class="modal-dialog modal-dialog-centered"
                                                                                role="document">
                                                                                <div class="modal-content">
                                                                                    <form method="POST"
                                                                                        action="/postdelete">
                                                                                        @csrf
                                                                                        <input type="hidden"
                                                                                            value="{{$data->id}}"
                                                                                            name="id">
                                                                                        <input type="hidden"
                                                                                            value="{{$data->image}}"
                                                                                            name="url">
                                                                                        <div
                                                                                            class="modal-body text-center bg-danger text-white">
                                                                                            Confirm to Delete
                                                                                        </div>
                                                                                        <div class="modal-footer"
                                                                                            style="text-align: center;  display: inherit;">
                                                                                            <button type="button"
                                                                                                class="btn btn-secondary"
                                                                                                data-dismiss="modal">No</button>
                                                                                            <button type="submit"
                                                                                                class="btn btn-danger">Yes</button>
                                                                                        </div>
                                                                                    </form>

                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <!--End User delete Confirm -->

                                                                    </div>
                                                                </div>


                                                                @endforeach
                                                            </div>
                                                        </div>

                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                </div>



                            </div>



                            <div class="col-xxl-12">
                                <h3>
News Feed</h3>
                                @if(count($newsfeeds_other) == 0)
                                No Post Found
                                @endif
                            </div>



                            @foreach($newsfeeds_other as $data)
                            <div class="col-lg-3 position-relative mb-3 col-sm-6 lazy" data-loader="showDiv">
                                <div class="card post-card">
                                    <div class="card-header">
                                        <h6 class="m-0">{{$data->fname}} {{$data->lname}} <span>{{$data->name}}</span>
                                        </h6><small class="post-time"> {{$data->created_at}} </small>
                                    </div>

                                    <div class="card-body">
                                        <div class="gallery">
                                            <a href="{{ asset('upload/blog/'.$data->image.'') }}" target="_blank">



                                                <div class="bg-image"
                                                    style="background-image:url({{ asset('upload/blog/'.$data->image.'') }})">
                                                </div>

                                                <div class="bg-text">

                                                    <!-- <h1 style="font-size:50px"> -->
                                                    <img
                                                            src="{{ asset('upload/blog/'.$data->image.'') }}" />
                                                        <!-- </h1> -->

                                                </div>

                                            </a>
                                        </div>
                                    </div>

                                    <div class="card-footer">
                                        <p class="card-text">{{$data->description}}</p>
                                    </div>

                                </div>
                            </div>

                            @endforeach






                        </div>
                    </div>

                </div>
            </div>
        </div>

    </div>





    @include('user-footer')


    <script src="{{ asset('assets/vendor/jquery/jquery.min.js')}}"></script>
    <script src="{{ asset('assets/vendor/bootstrap/bootstrap.min.js')}}"></script>
    <script src="{{ asset('assets/vendor/bootstrap/popper.min.js')}}"></script>
    <script src="{{ asset('assets/js/scripts.js')}}"></script>

    <script src="{{ asset('js/custom.js')}}?<?php echo time(); ?>"></script>

    <script type="text/javascript" src="{{ asset('js/jquery.lazy.min.js')}}"></script>

    <script>

$('.lazy').lazy({
  threshold: 0,
  showDiv: function(element, response) {
    element.css('opacity', 1);
    response(true);
  }
});

</script>

    <script src="{{ asset('assets/vendor/light-box/jquery.min.js')}}"></script>
    <script src="{{ asset('assets/vendor/light-box/jquery.lightbox.js')}}"></script>




    <script>

    (function($) {

        $.fn.imageUploadResizer = function(options) {
            var settings = $.extend({
                max_width: 1000,
                max_height: 1000,
                quality: 1,
                do_not_resize: [],
            }, options);

            this.filter('input[type="file"]').each(function() {
                this.onchange = function() {
                    const that = this; // input node
                    const originalFile = this.files[0];

                    if (!originalFile || !originalFile.type.startsWith('image')) {
                        return;
                    }

                    // Don't resize if doNotResize is set
                    if (settings.do_not_resize.includes('*') || settings.do_not_resize.includes(
                            originalFile.type.split('/')[1])) {
                        return;
                    }

                    var reader = new FileReader();

                    reader.onload = function(e) {
                        var img = document.createElement('img');
                        var canvas = document.createElement('canvas');

                        img.src = e.target.result
                        img.onload = function() {
                            var ctx = canvas.getContext('2d');
                            ctx.drawImage(img, 0, 0);

                            if (img.width < settings.max_width && img.height < settings
                                .max_height) {
                                // Resize not required
                                return;
                            }

                            const ratio = Math.min(settings.max_width / img.width, settings
                                .max_height / img.height);
                            const width = Math.round(img.width * ratio);
                            const height = Math.round(img.height * ratio);

                            canvas.width = width;
                            canvas.height = height;

                            var ctx = canvas.getContext('2d');
                            ctx.drawImage(img, 0, 0, width, height);

                            canvas.toBlob(function(blob) {
                                var resizedFile = new File([blob], 'cocacola_' +
                                    originalFile.name, originalFile);

                                var dataTransfer = new DataTransfer();
                                dataTransfer.items.add(resizedFile);

                                // temporary remove event listener, change and restore
                                var currentOnChange = that.onchange;

                                that.onchange = null;
                                that.files = dataTransfer.files;
                                that.onchange = currentOnChange;

                            }, 'image/jpeg', settings.quality);
                        }
                    }

                    reader.readAsDataURL(originalFile);
                }
            });

            return this;
        };

    }(jQuery));
    </script>

    <script>
    $(function() {
        $('.gallery a').lightbox();
    });
    </script>


    <script>
    var form = document.getElementById('form-submit');

    function myFunction() {
        if (form.checkValidity()) {
            $("#preloader").show();
            jQuery('#wait').html('Please wait, Uploading...');

        }
    }
    </script>


 
    <script>
    $(function() {
       
        $('#images-news').imageUploadResizer({

            max_width: 800, // Defaults 1000
            max_height: 800, // Defaults 1000
            quality: 0.8, // Defaults 1
            do_not_resize: ['gif', 'svg'], // Defaults []
        });

    });
    </script>

    @if(Session::has('message'))
    <script>
    $(document).ready(function() {
        $("#liveToast").toast('show');
    });
    </script>
    @endif

    <div class="position-fixed bottom-0 end-0 p-3" style="z-index: 11">

        <div class="toast align-items-center text-white bg-success border-0" role="alert" id="liveToast"
            aria-live="assertive" aria-atomic="true">
            <div class="d-flex">
                <div class="toast-body">
                    {!! session('message') !!}
                </div>
                <button type="button" class="btn-close btn-close-white me-2 m-auto" data-bs-dismiss="toast"
                    aria-label="Close"></button>
            </div>
        </div>

    </div>

    @if(Session::has('error'))
    <script>
    $(document).ready(function() {
        $("#liveToastre").toast('show');
    });
    </script>
    @endif

    <div class="position-fixed bottom-0 end-0 p-3" style="z-index: 11">

        <div class="toast align-items-center text-white bg-danger border-0" role="alert" id="liveToastre"
            aria-live="assertive" aria-atomic="true">
            <div class="d-flex">
                <div class="toast-body">
                    {!! session('error') !!}
                </div>
                <button type="button" class="btn-close btn-close-white me-2 m-auto" data-bs-dismiss="toast"
                    aria-label="Close"></button>
            </div>
        </div>

    </div>




    <script>
    $('textarea').keyup(function() {

        var characterCount = $(this).val().length,
            current = $('#current'),
            maximum = $('#maximum'),
            theCount = $('#the-count');

        current.text(characterCount);


        /*This isn't entirely necessary, just playin around*/
        if (characterCount < 70) {
            current.css('color', '#666');
        }
        if (characterCount > 70 && characterCount < 90) {
            current.css('color', '#6d5555');
        }
        if (characterCount > 90 && characterCount < 100) {
            current.css('color', '#793535');
        }
        if (characterCount > 100 && characterCount < 120) {
            current.css('color', '#841c1c');
        }
        if (characterCount > 120 && characterCount < 139) {
            current.css('color', '#8f0001');
        }

        if (characterCount >= 140) {
            maximum.css('color', '#8f0001');
            current.css('color', '#8f0001');
            theCount.css('font-weight', 'bold');
        } else {
            maximum.css('color', '#666');
            theCount.css('font-weight', 'normal');
        }


    });
    </script>





</body>

</html>