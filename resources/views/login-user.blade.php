<?php
$sid = session()->getId();
$user_id = session()->get('FRONT_USER_ID_COLA');
$user_name = session()->get('FRONT_USER_NAME_COLA');
$user_info = DB::table('users')->where(['id' => $user_id])->first();
$team = DB::table('teams')->where(['id' => $user_info->teams_id])->first();

$team_scores = DB::table('team_scores')->where(['team_id' => $user_info->teams_id])->first();
$user_scores = DB::table('user_scores')->where(['user_id' => $user_id])->first();


$t_score = 0;
if ($team_scores == '') {
    $t_score = 0;
} else {
    $t_score = $team_scores->score;
}

$u_score = 0;
if ($user_scores == '') {
    $u_score = 0;
} else {
    $u_score = $user_scores->score;
}
?>

<div class="col-xl-3 col-7">



    <div class="login-user">


        <a data-bs-toggle="modal" class="text-white" href="javascript:void(0);" data-bs-target="#logoutmodal"><div class="icon-menu"><i class="ri-logout-circle-line"></i></a>
            
        </div>

        <div class="user">
            <h5>{{$user_info->fname}} {{$user_info->lname}}</h5>
            <small>{{$team->name}}</small>
        </div>


    </div>


</div>
<div class="col-xl-3 col-5">

    <div class="stat-widget d-flex align-items-center">
        <div class="widget-icon me-2"><span><i class="ri-team-line"></i></span></div>
        <div class="widget-content">
            <h3>
                {{$t_score}}

            </h3>
            <p>My Team</p>
        </div>
    </div>

</div>

 <!-- User Delete Confirm -->
 <div class="modal fade" id="logoutmodal" tabindex="-1" role="dialog"
        aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <form method="get" action="/logout">
                    @csrf
                    <input type="hidden" id="imid" value="" name="imid">
                    <input type="hidden" id="imurl" value="" name="imurl">
                    <div class="modal-body text-center bg-danger text-white">
                        Confirm to Logout
                    </div>
                    <div class="modal-footer" style="text-align: center;  display: inherit;">
                    <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">No</button>
                        <button type="submit" class="btn btn-danger">Yes</button>
                    </div>
                </form>

            </div>
        </div>
    </div>
    <!--End User delete Confirm -->
