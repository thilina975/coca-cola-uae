<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Coca Cola</title>
    <!-- Favicon icon -->
    <link rel="icon" type="image/png" sizes="16x16" href="{{ asset('images/favicon.png')}}">
    <!-- Custom Stylesheet -->
    <link rel="stylesheet" href="{{ asset('assets/css/style.css')}}?<?php echo time(); ?>">
    <link rel="stylesheet" href="{{ asset('assets/css/custom.css')}}?<?php echo time(); ?>">
    <link href="https://fonts.googleapis.com/css2?family=Roboto:wght@400;900&display=swap" rel="stylesheet">
    <style>
    .card-title {
        font-size: 16px;
        margin-bottom: 0px;
        color: #e5192e;
    }


    .card {
        border: 1px solid #ffffff;
        margin-bottom: 10px;
        border-radius: 6px;
        background: #ffffff;
    }

    .card .card-body {
        padding: 10px 30px;
    }

    .btn-close1 {
        position: absolute;
        right: -3px;
        top: -13px;
        font-size: 23px;
        background: white;
        opacity: 1;
        line-height: 22px;
        border-radius: 50%;
        padding: 6px;
        width: 35px;
        height: 35px;
        border: 1px solid #d9d9d9
    }
    </style>
    <!-- <style type="text/css">
.g-recaptcha{
transform:scale(0.77);
-webkit-transform:scale(0.77);
transform-origin:0 0;
-webkit-transform-origin:0 0;
}
</style> -->
</head>

<body class="@@class" style="background-color:#e5192e;">

    <div id="preloader">
        <div><img src="{{ asset('images/loading.gif')}}"></div>
    </div>

    <div class="authincation">
        <div class="container position-absolute top-50 start-50 translate-middle">
            <div class="row justify-content-center h-100 align-items-center">

                <div class="col-12">
                    <div class="row">
                        <div class="col log-btn text-center">
                            <!-- <a target="_blank" href="/pdf/agenda">  -->

                            <button type="button" style="font-weight:bold" data-bs-toggle="modal" data-bs-target="#agenda"
                                class="btn btn-outline-light btn-sm">Agenda</button>
                            <!-- </a> -->

                            <!-- <a target="_blank" href="/pdf/teams">  -->
                            <button  style="font-weight:bold" type="button" data-bs-toggle="modal" data-bs-target="#teams"
                                class="btn btn-outline-light btn-sm">Teams</button>
                            <!-- </a> -->
                            <!-- <button type="button" disabled class="btn btn-outline-light btn-sm">xx</button> -->
                        </div>





                    </div>
                </div>
                <div class="col-xl-5 col-md-6 login-box">

                    <div class="row">
                     


                    
                        <div class="col-8 col-md-8" style="margin:0 auto;">
                            <div class="mini-logo text-right p-2">
                                <a href="#"><img style="width:100%;" src="{{ asset('images/logo.png')}}" alt=""></a>

                            </div>
                        </div>
                    </div>




                    <div class="auth-form card">
                        <!-- <h4 class="card-title text-center">Login</h4> -->
                        @if(Session::has('message'))


                        {!! Session()->get('message')!!}
                        @endif
                        <div class="card-body">
                            <form method="post" id="frmLogin" class="signin_validate row g-3">
                                @CSRF
                                <div class="col-12">
                                    <label class="form-label">Email</label>
                                    <input type="email" class="form-control" name="email" required>
                                    <span id="email_error" class="field_error"> </span>
                                </div>

                                <div class="form-group">
                                    <label>Password</label>
                                    <div class="input-group" id="show_hide_password">
                                        <input id="password" type="password" class="form-control" name="login_password"
                                            autocomplete="false" required>
                                        <div class="input-group-addon">
                                            <a href=""><i class="ri-eye-line"></i></a>
                                        </div>
                                    </div>
                                    <span id="login_password_error" class="field_error"> </span>
                                </div>





                                <div class="col-12 text-center">
                                    <button class="btn btn-danger disp" style="display: none;" id="load_login"
                                        type="button" disabled>
                                        <span class="spinner-grow spinner-grow-sm" role="status"
                                            aria-hidden="true"></span>
                                        Login...
                                    </button>
                                    <button type="submit" id="btnLogin" class="btn btn-danger">Login</button>
                                </div>

                                <div id="login_msg" class="m-0"></div>
                            </form>

                        </div>

                    </div>
                    <p class="text-center text-white m-0"><small>© 2022 Coca-Cola. All rights reserved</small></p>

                </div>
            </div>
        </div>
    </div>

    <!-- Modal -->
    <div class="modal fade" id="agenda" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content pdf-modal">

                <div class="modal-body">
                    <a class="btn-close1" data-bs-dismiss="modal" aria-label="Close"> <i
                            class="ri-close-circle-line"></i></a>
                    <!-- <iframe src="https://www.srilankamit.lk/pdf/agenda.pdf" frameborder="0" width="100%"
                                        style="height: 80vh;"></iframe> -->
                    <img style="width:100%" src="{{ asset('pdf/agenda-1.jpg')}}">
                </div>
                <!-- <div class="modal-footer">
                                    <button type="button" class="btn btn-secondary btn-sm"
                                        data-bs-dismiss="modal">Close</button>
                                   
                                </div> -->
            </div>
        </div>
    </div>

    <!-- Modal -->
    <div class="modal fade" id="teams" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content pdf-modal">

                <div class="modal-body">
                    <a class="btn-close1" data-bs-dismiss="modal" aria-label="Close"> <i
                            class="ri-close-circle-line"></i></a>
                    <!-- <iframe src="https://www.srilankamit.lk/pdf/agenda.pdf" frameborder="0" width="100%"
                                        style="height: 80vh;"></iframe> -->
                    <img style="width:100%" src="{{ asset('pdf/team-1-n.jpg')}}">
                    <img style="width:100%" src="{{ asset('pdf/team-2-n.jpg')}}">
                </div>
                <!-- <div class="modal-footer">
                                    <button type="button" class="btn btn-secondary btn-sm"
                                        data-bs-dismiss="modal">Close</button>
                                   
                                </div> -->
            </div>
        </div>
    </div>



    <script src="{{ asset('assets/vendor/jquery/jquery.min.js')}}"></script>
    <script src="{{ asset('assets/vendor/bootstrap/bootstrap.min.js')}}"></script>
    <script src="{{ asset('assets/vendor/bootstrap/popper.min.js')}}"></script>
    <script src="{{ asset('assets/js/scripts.js')}}"></script>
    <script src="{{ asset('js/custom.js')}}?<?php echo time();?>"></script>
</body>

</html>