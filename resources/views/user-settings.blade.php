<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Settings</title>
    <!-- Favicon icon -->
    <link rel="icon" type="image/png" sizes="16x16" href="./images/favicon.png">
    <!-- Custom Stylesheet -->
    <link rel="stylesheet" href="{{ asset('assets/css/style.css')}}">
    <link rel="stylesheet" href="{{ asset('assets/css/custom.css')}}">
    <link rel="stylesheet" href="{{ asset('assets/vendor/select2/select2.min.css') }}">
    <link rel="stylesheet" href="{{ asset('vendors/fontawesome/css/all.min.css')}}">
</head>

<body class="dashboard">
    <div id="preloader">
        <i>.</i>
        <i>.</i>
        <i>.</i>
    </div>
    <div id="main-wrapper">



        @include('user-header')

        <div class="content-body">
            <div class="container">
                <div class="row">
                    <div class="col-xl-12">
                        <div class="page-title">
                            <div class="row align-items-center justify-content-between">
                                <div class="col-xl-4">
                                    <div class="page-title-content">
                                        <h3>Settings</h3>
                                        <div class="breadcrumbs"><a href="#">Dashboard </a><span><i
                                                    class="ri-arrow-right-s-line"></i></span><a href="#">Settings</a>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-auto">
                                    @include('login-user')

                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-xxl-12 col-xl-12">

                        <div class="row">

                            <div class="col-xxl-6 col-xl-6 col-lg-6">
                                <div class="card">
                                    <div class="card-header">
                                        <h4 class="card-title">Change Password</h4>
                                    </div>
                                    <div class="card-body">
                                        <form id="frmuserchangepw">
                                            @csrf
                                            <div class="row g-3">
                                                <div id="cpw_msg"></div>

                                                <div class="form-group">
                                                    <label>Current Password</label>
                                                    <div class="input-group" id="show_hide_password">
                                                        <input id="current_password" placeholder="**********" type="password"
                                                            class="form-control" name="current_password" autocomplete="false"
                                                            required>
                                                        <div class="input-group-addon">
                                                            <a href=""><i class="fa fa-eye-slash"
                                                                    aria-hidden="true"></i></a>
                                                        </div>
                                                    </div>

                                                    <span id="current_password_error" class="field_error"> </span>
                                                </div>

                                                <div class="form-group">
                                                    <label>New Password</label>
                                                    <div class="input-group" id="show_hide_password">
                                                        <input id="password" placeholder="**********" type="password"
                                                            class="form-control" name="password" autocomplete="false"
                                                            required>
                                                        <div class="input-group-addon">
                                                            <a href=""><i class="fa fa-eye-slash"
                                                                    aria-hidden="true"></i></a>
                                                        </div>
                                                    </div>

                                                    <span id="password_error" class="field_error"> </span>
                                                </div>
                                                <div class="form-group">
                                                    <label>Retype Password</label>
                                                    <div class="input-group" id="show_hide_password">
                                                        <input id="password_confirmation" placeholder="**********"
                                                            type="password" class="form-control"
                                                            name="password_confirmation" required>
                                                        <div class="input-group-addon">
                                                            <a href=""><i class="fa fa-eye-slash"
                                                                    aria-hidden="true"></i></a>
                                                        </div>
                                                    </div>
                                                    <span id="password_confirmation_error" class="field_error"> </span>
                                                </div>
                                                
                                                <div class="col-12 col-12 mb-3">
                                                <button class="btn btn-primary disp" style="display: none;" id="loadcpw" type="button"
                                disabled>
                                <span class="spinner-grow spinner-grow-sm" role="status" aria-hidden="true"></span>
                                Changing...
                            </button>
                                                    <button type="submit" class="btn btn-primary" id="changepwbtn">Change</button>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>



                        </div>
                    </div>

                </div>
            </div>
        </div>

    </div>

    @include('user-footer')


    <script src="{{ asset('assets/vendor/jquery/jquery.min.js')}}"></script>
    <script src="{{ asset('assets/vendor/bootstrap/js/bootstrap.bundle.min.js')}}"></script>
    <script src="{{ asset('assets/js/scripts.js')}}"></script>
    <script src="{{ asset('assets/vendor/select2/select2.full.min.js') }}"></script>
    <script src="{{ asset('js/custom.js')}}?<?php echo time(); ?>"></script>
    <script>
    $(function() {
        $(".select2").select2();
        allowClear: false

    });
    $.fn.modal.Constructor.prototype.enforceFocus = function() {};
    </script>
    @if(Session::has('message'))
    <script>
    $(document).ready(function() {
        $("#liveToast").toast('show');
    });
    </script>
    @endif

    <div class="position-fixed bottom-0 end-0 p-3" style="z-index: 11">

        <div class="toast align-items-center text-white bg-success border-0" role="alert" id="liveToast"
            aria-live="assertive" aria-atomic="true">
            <div class="d-flex">
                <div class="toast-body">
                    {!! session('message') !!}
                </div>
                <button type="button" class="btn-close btn-close-white me-2 m-auto" data-bs-dismiss="toast"
                    aria-label="Close"></button>
            </div>
        </div>

    </div>




</body>

</html>