<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Reset Your Password</title>
    <!-- Favicon icon -->
    <link rel="icon" type="image/png" sizes="16x16" href="./images/favicon.png">
    <!-- Custom Stylesheet -->
    <link rel="stylesheet" href="{{ asset('assets/css/style.css')}}">
    <link rel="stylesheet" href="{{ asset('vendors/fontawesome/css/all.min.css')}}">
    <!-- <link rel="stylesheet" href="{{ asset('vendors/bootstrap/css/bootstrap.css')}}"> -->
</head>

<body class="@@class">

    <div id="preloader">
        <i>.</i>
        <i>.</i>
        <i>.</i>
    </div>

    <div class="authincation">
        <div class="container h-100">
            <div class="row justify-content-center h-100 align-items-center">
                <div class="col-xl-5 col-md-6">
                    <div class="mini-logo text-center my-4">
                        <a href="#"><img style="width: 120px;" src="{{ asset('images/logo.png')}}" alt=""></a>

                    </div>
                    <div class="auth-form card">
                        <h4 class="card-title text-center">Reset Your Password</h4>
                        <p class="text-center">{{session()->get('FORGOT_PASSWORD_EMAIL')}}</p>
                        <div class="card-body">
                            <form method="post" id="frmUserUpdatePassword" class="signin_validate row g-3">
                                @CSRF
                            

                                <div class="form-group">
                                    <label>New Password</label>
                                    <div class="input-group" id="show_hide_password">
                                        <input id="password" type="password" class="form-control"
                                            name="password" autocomplete="false" required>
                                        <div class="input-group-addon">
                                            <a href=""><i class="fa fa-eye-slash" aria-hidden="true"></i></a>
                                        </div>
                                    </div>

                                    <span id="password_error" class="field_error"> </span>
                                </div>
                                <div class="form-group">
                                    <label>Retype Password</label>
                                    <div class="input-group" id="show_hide_password">
                                        <input id="password_confirmation" type="password" class="form-control"
                                            name="password_confirmation" required>
                                        <div class="input-group-addon">
                                            <a href=""><i class="fa fa-eye-slash" aria-hidden="true"></i></a>
                                        </div>
                                    </div>
                                    <span id="password_confirmation_error" class="field_error"> </span>
                                </div>

                             
                                <button class="btn btn-primary disp" style="display: none;" id="load_fc"
                                    type="button" disabled>
                                    <span class="spinner-grow spinner-grow-sm" role="status" aria-hidden="true"></span>
                                    Password Changing...
                                </button>
                                <div class="d-grid gap-2">
                                    <button type="submit" id="fcbt" class="btn btn-primary">Change Password</button>
                                </div>

                                <div id="login_msg"></div>
                            </form>
                           
                        </div>

                    </div>
                    <p class="text-center"><small>© 2022 ibeclk.org. All rights reserved</small></p>

                </div>
            </div>
        </div>
    </div>





    <script src="{{ asset('assets/vendor/jquery/jquery.min.js')}}"></script>
    <script src="{{ asset('vendors/bootstrap/js/bootstrap.min.js')}}"></script>
    <script src="{{ asset('assets/js/scripts.js')}}"></script>
    <script src="{{ asset('js/custom.js')}}?<?php echo time();?>"></script>
</body>

</html>