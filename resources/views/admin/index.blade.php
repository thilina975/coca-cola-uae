<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Admin Login</title>
    <!-- Favicon icon -->
    <link rel="icon" type="image/png" sizes="16x16" href="./images/favicon.png">
    <!-- Custom Stylesheet -->
    <link rel="stylesheet" href="{{ asset('assets/css/style.css')}}">
    <link rel="stylesheet" href="{{ asset('vendors/fontawesome/css/all.min.css')}}">
    <!-- <link rel="stylesheet" href="{{ asset('vendors/bootstrap/css/bootstrap.css')}}"> -->
</head>

<body class="bg-danger">

    <div id="preloader">
        <i>.</i>
        <i>.</i>
        <i>.</i>
    </div>

    <div class="authincation">
        <div class="container h-100">
            <div class="row justify-content-center h-100 align-items-center">
                <div class="col-xl-5 col-md-6">
                    <div class="mini-logo text-center my-4">
                        <a href="#"><img style="width: 120px;" src="{{ asset('images/logo.png')}}" alt=""></a>

                    </div>
                    <div class="auth-form card">
                        <h4 class="card-title text-center">Admin Login  {{config('app.adminurl')}}</h4>
                        @if(Session::has('message'))


                        {!! Session()->get('message')!!}
                        @endif
                        <div class="card-body">
                            <form method="post" action="/adminlogin" class="signin_validate row g-3">
                                @CSRF
                                <div class="col-12">
                                    <label class="form-label">Email</label>
                                    <input type="email" class="form-control"
                                        name="email" required>
                                </div>

                                <div class="form-group">
                                    <label>Password</label>
                                    <div class="input-group" id="show_hide_password">
                                        <input id="password" type="password" class="form-control" name="login_password"
                                            autocomplete="false" required>
                                        <div class="input-group-addon">
                                            <a href=""><i class="fa fa-eye-slash" aria-hidden="true"></i></a>
                                        </div>
                                    </div>
                                </div>

                              
                                <div class="col-6">
                                    <div class="form-check form-switch">
                                        <input class="form-check-input" type="checkbox" id="flexSwitchCheckDefault">
                                        <label class="form-check-label" for="flexSwitchCheckDefault">Remember
                                            me</label>
                                    </div>
                                </div>
                                <div class="col-6 text-end">
                                    <a data-bs-toggle="modal" href="javascript:void(0);"
                                        data-bs-target="#pwresetpopup">Forgot Password?</a>
                                </div>
                                <button class="btn btn-primary disp" style="display: none;" id="load_login"
                                    type="button" disabled>
                                    <span class="spinner-grow spinner-grow-sm" role="status" aria-hidden="true"></span>
                                    Login...
                                </button>
                                <div class="d-grid gap-2">
                                    <button type="submit" id="btnLogin" class="btn btn-primary">Login</button>
                                </div>

                              
                            </form>
                      
                        </div>

                    </div>
                    <p class="text-center"><small>© 2022 coca-cola. All rights reserved</small></p>

                </div>
            </div>
        </div>
    </div>




    <!-- otp  -->
    <div class="modal fade" id="pwresetpopup" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1"
        aria-labelledby="staticBackdropLabel" aria-hidden="true">
        <div class="modal-dialog  modal-dialog-centered">
            <div class="modal-content email-otp-modal">
                <div class="modal-header bg-primary text-center">
                    <h5 class="modal-title text-center" id="RegisterModalLabel">Forgot Password</h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body">
                    <p class="text-center">Enter your registerd email address below and we'll send you password reset
                        link</p>
                    <form method="POST" id="frmFogot" class="text-center">
                        @CSRF
                        <div class="form-group">
                            <label class="wo-titleinput">Your Email ID:</label>
                            <input type="email" class="form-control" id="email" name="email" placeholder="Enter email*"
                                required>
                            <span id="email_error" class="field_error"> </span>
                        </div>


                        <div class="form-group text-center mt-4">
                            <button class="btn btn-primary disp" style="display: none;" id="loadfogot" type="button"
                                disabled>
                                <span class="spinner-grow spinner-grow-sm" role="status" aria-hidden="true"></span>
                                sending...
                            </button>
                            <button type="submit" id="btnfogot" class="btn btn-primary">Send</button>
                        </div>

                        <div id="forgot_msg" class="mt-3 text-center"></div>
                    </form>

                </div>
            </div>
        </div>
    </div>

    <!-- end otp  -->



    <script src="{{ asset('assets/vendor/jquery/jquery.min.js')}}"></script>
    <script src="{{ asset('vendors/bootstrap/js/bootstrap.min.js')}}"></script>
    <script src="{{ asset('assets/js/scripts.js')}}"></script>
    <script src="{{ asset('js/custom.js')}}?<?php echo time();?>"></script>
</body>

</html>