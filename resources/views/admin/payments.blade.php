<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Payments</title>
    <!-- Favicon icon -->
    <link rel="icon" type="image/png" sizes="16x16" href="./images/favicon.png">
    <!-- Custom Stylesheet -->
    <link rel="stylesheet" href="{{ asset('admin/css/style.css')}}">
    <link rel="stylesheet" href="{{ asset('admin/css/custom.css')}}">

</head>

<body class="dashboard">

    <div id="preloader">
        <i>.</i>
        <i>.</i>
        <i>.</i>
    </div>

    <div id="main-wrapper">



        @include('user-header')

        <div class="content-body">
            <div class="container">
                <div class="row">
                    <div class="col-xl-12">
                        <div class="page-title">
                            <div class="row align-items-center justify-content-between">
                                <div class="col-xl-4">
                                    <div class="page-title-content">
                                        <h3>Payments</h3>
                                        <div class="breadcrumbs"><a href="#">Dashboard </a><span><i
                                                    class="ri-arrow-right-s-line"></i></span><a href="#">Payments</a>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-auto">
                                    @include('login-user')

                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-xxl-12 col-xl-12 mt-4">

                        <div class="row">

                            <div class="card">

                                <div class="card-body">

                                   <link href="{{ asset('admin/vendor/datatables/datatables.css')}}" type="text/css"
                                        rel="stylesheet">
                                    <link rel="stylesheet" type="text/css"
                                        href="{{ asset('admin/vendor/datatables/fixedHeader.dataTables.min.css')}}">
                                    <table id="example" class="table  table-striped table-bordered display"
                                        style="width:100%">
                                        <thead>
                                            <tr>
                                                <th>Invoice ID</th>
                                                <th>Date</th>
                                                <th>Amount</th>
                                                <th>Payment Method</th>

                                                <th>Status</th>



                                            </tr>
                                        </thead>
                                        <tbody>

                                            @foreach($payments as $list)
                                            <tr>
                                                <td>{{$list->invoice_id}}</td>
                                                <td>{{$list->created_at}}</td>
                                                <td><?php echo number_format($list->amount, 2) ?></td>
                                                <td>{{$list->payment_method}}</td>
                                                <td>{{$list->payments_status_message}}</td>




                                            </tr>
                                            @endforeach


                                        </tbody>

                                    </table>
                                    
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>

    </div>

    @include('user-footer')


    <script src="{{ asset('admin/vendor/jquery/jquery.min.js')}}"></script>
    <script src="{{ asset('admin/vendor/bootstrap/js/bootstrap.bundle.min.js')}}"></script>
    <script src="{{ asset('admin/js/scripts.js')}}"></script>
    <script src="{{ asset('admin/vendor/datatables/datatables.min.js')}}"></script>
    <script type="text/javascript" src="{{ asset('admin/vendor/datatables/dataTables.fixedHeader.min.js')}}"></script>
    <script>
    $(document).ready(function() {
        $('#example').DataTable({
            fixedHeader: true,
            "order": []
        });
    });
    </script>

</body>

</html>