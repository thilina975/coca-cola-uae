<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Payments</title>
    <!-- Favicon icon -->
    <link rel="icon" type="image/png" sizes="16x16" href="./images/favicon.png">
    <!-- Custom Stylesheet -->
    <link rel="stylesheet" href="{{ asset('assets/css/style.css')}}">
    <link rel="stylesheet" href="{{ asset('assets/css/custom.css')}}">

</head>

<body class="dashboard">

    <div id="preloader">
        <i>.</i>
        <i>.</i>
        <i>.</i>
    </div>

    <div id="main-wrapper">




        <div class="content-body">
            <div class="container">
                <div class="row">
                    <div class="col-xl-12">
                        <div class="page-title mt-3">
                            <div class="row align-items-center justify-content-between">

                                <div class="page-title-content">
                                    <h3>Users</h3>
                                    <hr>
                                </div>


                            </div>
                        </div>
                    </div>
                    <div class="col-xxl-12 col-xl-12 mt-4">

                        <div class="row">


                            <div class="col-md-4">


                                <form method="POST" action="/admin/add-users">
                                    @csrf

                                    <input type="hidden" name="id" value="{{$id}}">

                                    <div class="mb-3">
                                        <label for="exampleInputEmail1" class="form-label">First name</label>
                                        <input type="text" name="fname" value="{{$fname}}" class="form-control"
                                            required>

                                    </div>
                                    <div class="mb-3">
                                        <label for="exampleInputEmail1" class="form-label">Last name</label>
                                        <input type="text" name="lname" value="{{$lname}}" class="form-control"
                                            required>

                                    </div>
                                    <div class="mb-3">
                                        <label for="exampleInputPassword1" class="form-label">Email</label>
                                        <input type="email" class="form-control" value="{{$email}}" name="email">
                                        @error('email')
                                        <small>{{ $message }}</small>
                                        @enderror
                                    </div>

                                    <div class="mb-3">
                                        <label for="exampleInputPassword1" class="form-label">Password</label>
                                        <input type="text" class="form-control" value="{{$password}}" name="password">
                                    </div>




                                    <div class="mb-3">
                                        <label for="exampleInputPassword1" class="form-label">Team</label>
                                        <select name="team" class="form-control" required>
                                            <option value="">Select Team</option>
                                            @foreach($teams as $ans)
                                            <option <?php if($team == $ans->id){echo 'selected';}?>
                                                value="{{$ans->id}}">
                                                {{$ans->name}}
                                            </option>


                                            @endforeach
                                        </select>
                                    </div>
                                    <button type="submit" class="btn btn-primary">Submit</button>
                                </form>
                            </div>
                            <div class="col-md-8">



                                <link href="{{ asset('assets/vendor/datatables/datatables.css')}}" type="text/css"
                                    rel="stylesheet">
                                <link rel="stylesheet" type="text/css"
                                    href="{{ asset('assets/vendor/datatables/fixedHeader.dataTables.min.css')}}">
                                <table id="example" class="table  table-striped table-bordered display"
                                    style="width:100%">
                                    <thead>
                                        <tr>
                                            <th>ID</th>
                                            <th>First Name</th>
                                            <th>Last Name</th>

                                            <th>Email</th>
                                            <th>password</th>
                                            <th>Team</th>
                                            <th></th>


                                        </tr>
                                    </thead>
                                    <tbody>

                                        @foreach($users as $list)
                                        <tr>
                                            <td>{{$list->id}}</td>
                                            <td>{{$list->fname}}</td>
                                            <td>{{$list->lname}}</td>


                                            <td>{{$list->email}}</td>
                                            <td>{{$list->password}}</td>
                                            <td>{{$list->teams_id}}</td>

                                            <td><a href="/admin/users/{{$list->id}}">edit</a></td>


                                        </tr>
                                        @endforeach


                                    </tbody>

                                </table>


                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>

    </div>




    <script src="{{ asset('assets/vendor/jquery/jquery.min.js')}}"></script>
    <script src="{{ asset('assets/vendor/bootstrap/js/bootstrap.bundle.min.js')}}"></script>
    <script src="{{ asset('assets/js/scripts.js')}}"></script>
    <script src="{{ asset('assets/vendor/datatables/datatables.min.js')}}"></script>
    <script type="text/javascript" src="{{ asset('assets/vendor/datatables/dataTables.fixedHeader.min.js')}}"></script>
    <script>
    $(document).ready(function() {
        $('#example').DataTable({
            fixedHeader: true,
            "order": []
        });
    });
    </script>

</body>

</html>