<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Activity</title>
    <!-- Favicon icon -->
    <link rel="icon" type="image/png" sizes="16x16" href="{{ asset('images/favicon.png')}}">
    <!-- Custom Stylesheet -->
    <link rel="stylesheet" href="{{ asset('assets/css/style.css')}}">
    <link rel="stylesheet" href="{{ asset('assets/css/custom.css')}}">
    <link rel="stylesheet" href="{{ asset('assets/vendor/select2/select2.min.css') }}">
    <link rel="stylesheet" href="{{ asset('vendors/fontawesome/css/all.min.css')}}">
    <link rel="stylesheet" href="{{ asset('assets/vendor/datepicker/datepicker3.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/vendor/light-box/jquery.lightbox.css') }}">

    <style>
    .card .card-body {
        background: white;
    }

    .card {
        border: 1px solid #e5192e;
    }
    </style>
</head>

<body class="dashboard">

    <div id="preloader">
        <div><img src="{{ asset('images/loading.gif')}}"></div>
    </div>
    <div id="main-wrapper">

        <?php

$user_id = session()->get('FRONT_USER_ID_COLA');
?>

        @include('user-header')

        <div class="content-body">
            <div class="container">
                <div class="row">
                    <div class="col-xl-12">
                        <div class="page-title">
                            <div class="row align-items-center justify-content-between">
                                @include('login-user')
                            </div>
                        </div>
                    </div>


                    <div class="col-xxl-12 col-xl-12">

                        <div class="row">

                            <div class="col-xxl-12">


                                <div class="card">
                                    <div class="card-header">
                                        <h4 class="card-title"><a href="/outlet/{{$outlet->id}}"
                                                data-bs-toggle="tooltip" data-bs-placement="top"
                                                title="Go to {{$outlet->name}}" class="text-white"> {{$outlet->name}}
                                            </a></h4>
                                    </div>

                                    <div class="card-body">
                                        <form action="/activity-answer" method="POST" enctype="multipart/form-data">
                                            @CSRF

                                            <div class="row bg-white">

                                                <div class="accordion" id="accordionExample">
                                                    <h2 class="text-muted">Activity {{$activity_details->id}}</h2>
                                                    <h4>
                                                        {{$activity_details->question}}
                                                    </h4>
                                                    <input type="hidden" value="{{$activity_details->id}}"
                                                        name="q_id" />
                                                    <input type="hidden" value="{{$outlet->id}}" name="outlet_id" />

                                                    @if($activity_details->questionType->question_type == 'DROPDOWN')
                                                    <select required="true" name="selected_answer" class="form-control">
                                                        @foreach($question_answers_list as $data)
                                                        <option
                                                            <?php if ($already != '') {if ($already->answers_id == $data->id) {echo "selected";}}?>
                                                            value="{{$data->id}}">{{$data->answer}}</option>


                                                        @endforeach
                                                    </select>
                                                    @elseif($activity_details->questionType->question_type == 'RADIO')
                                                    @foreach($question_answers_list as $data)
                                                    <div class="form-check">
                                                        <input value="{{$data->id}}" class="form-check-input"
                                                            type="radio" name="selected_answer" id="flexRadioDefault1" <?php
if ($already != '') {
    if ($already->answers_id == $data->id) {echo "checked";}}
?>>
                                                        <label class="form-check-label" for="selected_answer">
                                                            {{$data->answer}}
                                                        </label>
                                                    </div>


                                                    @endforeach

                                                    @elseif($activity_details->questionType->question_type ==
                                                    'RADIO_AND_IMAGE')
                                                    @foreach($question_answers_list as $data)
                                                    <div class="form-check">
                                                        <input value="{{$data->id}}" class="form-check-input"
                                                            type="radio" name="selected_answer" onclick="<?php
                                                             if ($data->answer == "Yes") {
                                                                if(isset($asnimages[0])){
                                                                    
                                                                
                                                                }else{
                                                                   echo "showFileInput()";  
                                                                }
                                                            } else {echo "hideFileInput()";}
                                                            
                                                            ?>" <?php
if ($already != '') {
    if ($already->answers_id == $data->id) {echo "checked";}}
?>>
                                                        <label value="" class="form-check-label"
                                                            for="flexRadioDefault1">
                                                            {{$data->answer}}
                                                        </label>
                                                    </div>

                                                    @endforeach
                                                    <!-- <div>
                                                        <input class="form-check-input" type="radio" name="chooseNo"
                                                            id="chooseNo" onclick="">
                                                        <label class="form-check-label" for="flexRadioDefault1">  No
                                                        </label>
                                                    </div> -->
                                                    <input type="file" class="form-control mt-3" name="imagefile_1[]"
                                                        id="imageFile_1" value="Show Div" style="<?php
if ($data->answer == "Yes") {
    if(isset($asnimages[0])){
        echo "display:none;"; 

    }else{
        echo "display:block;";
    }
  
}else{
echo "display:none;";
}

?> " />
                                                    @if(isset($asnimages[0]))
                                                    <div class="col-md-12 col-12 mt-3 ">

                                                        <div class="row gallery">
                                                            @foreach($asnimages as $data)
                                                            <div class="col-xl-3 col-lg-3 col-md-3 col-sm-6 p-0">

                                                                <a href="{{ asset('upload/question/'.$data->image.'') }}"
                                                                    target="_blank">
                                                                    <img src="{{ asset('upload/question/'.$data->image.'') }}"
                                                                        class="img-fluid" />
                                                                </a>

                                                            </div>
                                                            @endforeach

                                                        </div>



                                                    </div>
                                                    @endif

                                                    @elseif($activity_details->questionType->question_type == 'FILE')
                                                    <div class="row">
                                                        <div class="col-md-12 col-12">
                                                            <lable>Maximum 4 images</lable>
                                                            <input type="hidden" name="selected_answer"
                                                                value="{{$question_answers_list[0]->id}}">
                                                            <input type="file" id="imageFile" class="form-control"
                                                                required name="imageFile[]" multiple required>
                                                        </div>
                                                        <div class="col-md-12 col-12 mt-3">

                                                            <div class="row gallery">
                                                                @foreach($asnimages as $data)
                                                                <div class="col-xl-3 col-lg-3 col-md-3 col-sm-6">
                                                                    <div class="stat-widget d-flex align-items-center">
                                                                        <a href="{{ asset('upload/question/'.$data->image.'') }}"
                                                                            target="_blank">
                                                                            <img src="{{ asset('upload/question/'.$data->image.'') }}"
                                                                                class="img-fluid" />
                                                                        </a>
                                                                    </div>
                                                                </div>
                                                                @endforeach

                                                            </div>



                                                        </div>
                                                    </div>


                                                    @elseif($activity_details->questionType->question_type ==
                                                    'RADIO_AND_IMAGE')

                                                    @endif
                                                </div>

                                            </div>




                                            @if($already =='')

                                            <div class="col-12 mt-4">
                                                <button type="submit" id="btnSave" class="btn btn-danger">Save</button>

                                            </div>

                                            @else
                                            @if($already->users_id == $user_id)
                                            <div class="col-12 mt-4">
                                                <button type="submit" id="btnSave" class="btn btn-danger">Save</button>

                                            </div>
                                            @endif

                                            @endif

                                        </form>
                                    </div>
                                </div>
                            </div>






                        </div>
                    </div>

                </div>
            </div>
        </div>

    </div>


    <div class="modal fade" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered">
            <div class="modal-content">

                <div class="modal-body text-center">
                    <h1><i class="ri-error-warning-line text-danger"></i>
                    </h1>
                    <p>You can select only 4 images</p>
                    <button type="button" class="btn btn-warning" data-bs-dismiss="modal">Ok</button>
                </div>

            </div>
        </div>
    </div>


    @include('user-footer')


    <script src="{{ asset('assets/vendor/light-box/jquery.min.js')}}"></script>
    <script src="{{ asset('assets/vendor/bootstrap/bootstrap.min.js')}}"></script>
    <script src="{{ asset('assets/vendor/bootstrap/popper.min.js')}}"></script>
    <script src="{{ asset('assets/js/scripts.js')}}"></script>

    <script src="{{ asset('js/custom.js')}}?<?php echo time(); ?>"></script>

    <script src="{{ asset('assets/vendor/light-box/jquery.lightbox.js')}}"></script>
    <script>
    // Initiate Lightbox
    $(function() {
        $('.gallery a').lightbox();
    });

    $("#imageFile").on("change", function() {
        if ($("#imageFile")[0].files.length > 4) {
            $('#exampleModal').modal('show');
            $('#imageFile').val('');
        }
    });
    </script>

    <script>
    $(function() {
        $('[data-toggle="tooltip"]').tooltip()
    })
    </script>

    @if(Session::has('message'))
    <script>
    $(document).ready(function() {
        $("#liveToast").toast('show');
    });
    </script>
    @endif

    <div class="position-fixed bottom-0 end-0 p-3" style="z-index: 11">

        <div class="toast align-items-center text-white bg-success border-0" role="alert" id="liveToast"
            aria-live="assertive" aria-atomic="true">
            <div class="d-flex">
                <div class="toast-body">
                    {!! session('message') !!}
                </div>
                <button type="button" class="btn-close btn-close-white me-2 m-auto" data-bs-dismiss="toast"
                    aria-label="Close"></button>
            </div>
        </div>

    </div>
    @if(Session::has('error'))
    <script>
    $(document).ready(function() {
        $("#liveToastre").toast('show');
    });
    </script>
    @endif

    <div class="position-fixed bottom-0 end-0 p-3" style="z-index: 11">

        <div class="toast align-items-center text-white bg-danger border-0" role="alert" id="liveToastre"
            aria-live="assertive" aria-atomic="true">
            <div class="d-flex">
                <div class="toast-body">
                    {!! session('error') !!}
                </div>
                <button type="button" class="btn-close btn-close-white me-2 m-auto" data-bs-dismiss="toast"
                    aria-label="Close"></button>
            </div>
        </div>

    </div>



    <script>
    function showFileInput() {
        document.getElementById('imageFile_1').style.display = "block";
    }

    function hideFileInput() {
        document.getElementById('imageFile_1').style.display = "none";
    }
    </script>

</body>

</html>