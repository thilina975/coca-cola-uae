

jQuery('#frmLogin').submit(function (e) {
    jQuery('#login_msg').html("");
    e.preventDefault();

    jQuery('.field_error').html('');

    $("#load_login").show();
    $("#btnLogin").hide();

    jQuery.ajax({

        url: '/login_process',
        data: jQuery('#frmLogin').serialize(),
        type: 'post',
        success: function (result) {
            if (result.status == "error") {

                jQuery.each(result.error, function (key, val) {
                    jQuery('#' + key + '_error').html(val[0]);
                });
                jQuery('#login_msg').html(result.msg);
                $("#load_login").hide();
                $("#btnLogin").show();
            }


            if (result.status == "success") {
                // alert(result.status);
                window.location.href = "/outlets";

            }

        }
    });
});



function nl2br(str, is_xhtml) {
    if (typeof str === 'undefined' || str === null) {
        return '';
    }
    var breakTag = (is_xhtml || typeof is_xhtml === 'undefined') ? '<br />' : '<br>';
    return (str + '').replace(/([^>\r\n]?)(\r\n|\n\r|\r|\n)/g, '$1' + breakTag + '$2');
}

$(function(){

    let path = window.location.href;
    $('#adminnav li a').each(function() {
        if (this.href === path) {
            $(this).addClass('active');
        }else{
            $(this).removeClass('active')
        }
        
    })
    
});

$(function(){

    let path = window.location.href;
    $('#dashmenu li a').each(function() {
        if (this.href === path) {
            $(this).addClass('active');
        }else{
            $(this).removeClass('active')
        }
        
    })
    
});

$(function () {

    var current = location.pathname;
    // alert(current);
    $('#dashboard-menu ul li').each(function () {
        var $this = $(this);
        // if the current path is like this link, make it active
        $this.removeClass('active');
        if ($this.attr('id').indexOf(current) !== -1) {
            $this.addClass('active');
        }
    })
})




$(document).ready(function() {
    $("#show_hide_password a").on('click', function(event) {
        event.preventDefault();
        if($('#show_hide_password input').attr("type") == "text"){
            $('#show_hide_password input').attr('type', 'password');
            $('#show_hide_password i').addClass( "ri-eye-line" );
            $('#show_hide_password i').removeClass( "ri-eye-off-line" );
        }else if($('#show_hide_password input').attr("type") == "password"){
            $('#show_hide_password input').attr('type', 'text');
            $('#show_hide_password i').removeClass( "ri-eye-line" );
            $('#show_hide_password i').addClass( "ri-eye-off-line" );
        }
    });
});

$(function () {
    'use strict';

    var body = $('#verifiation');

    function goToNextInput(e) {
        var key = e.which,
            t = $(e.target),
            sib = t.next('input');

        if (key != 9 && (key < 48 || key > 57)) {
            e.preventDefault();
            return false;
        }

        if (key === 9) {
            return true;
        }

        if (!sib || !sib.length) {
            sib = body.find('input').eq(0);
        }
        sib.select().focus();
    }

    function onKeyDown(e) {
        var key = e.which;

        if (key === 9 || (key >= 48 && key <= 57)) {
            return true;
        }

        e.preventDefault();
        return false;
    }

    function onFocus(e) {
        $(e.target).select();
    }

    body.on('keyup', 'input', goToNextInput);
    body.on('keydown', 'input', onKeyDown);
    body.on('click', 'input', onFocus);

})




